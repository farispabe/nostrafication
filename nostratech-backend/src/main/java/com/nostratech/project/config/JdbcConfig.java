package com.nostratech.project.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class JdbcConfig {

    @Value("${database.driverClassName}")
    String driverClassName;

    @Value("${database.url}")
    String url;

    @Value("${database.username}")
    String username;

    @Value("${database.password}")
    String password;

    @Value("${database.connectionTimeout}")
    Integer connectionTimeout;

    @Value("${database.idleTimeout}")
    Integer idleTimeout;

    @Value("${database.maxLifetime}")
    Integer maxLifetime;

    @Value("${database.maximumPoolSize}")
    Integer maximumPoolSize;

    @Bean(name = "mainDataSource", destroyMethod = "close")
    public DataSource dataSource(){
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setDriverClassName(driverClassName);
        hikariDataSource.setJdbcUrl(url);
        hikariDataSource.setUsername(username.trim());
        hikariDataSource.setPassword(password.trim());
        hikariDataSource.setConnectionTimeout(connectionTimeout);
        hikariDataSource.setIdleTimeout(idleTimeout);
        hikariDataSource.setMaxLifetime(maxLifetime);
        hikariDataSource.setMaximumPoolSize(maximumPoolSize);

        return hikariDataSource;
    }
}
