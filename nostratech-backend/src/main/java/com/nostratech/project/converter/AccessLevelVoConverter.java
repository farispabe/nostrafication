package com.nostratech.project.converter;

import com.nostratech.project.persistence.domain.AccessLevel;
import com.nostratech.project.util.ExtendedSpringBeanUtil;
import com.nostratech.project.vo.AccessLevelVO;
import org.springframework.stereotype.Component;

/**
 * Created by PanjiAlam on 8/16/2016.
 */

@Component
public class AccessLevelVoConverter extends BaseVoConverter<AccessLevelVO, AccessLevelVO, AccessLevel> implements IBaseVoConverter<AccessLevelVO, AccessLevelVO, AccessLevel>{

    @Override
    public AccessLevelVO transferModelToVO(AccessLevel model, AccessLevelVO vo){

        if (null == vo) vo = new AccessLevelVO();
        super.transferModelToVO(model, vo);

        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"instance","value", "keyName"});

        return vo;

    }

}
