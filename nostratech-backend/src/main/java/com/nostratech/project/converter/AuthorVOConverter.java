package com.nostratech.project.converter;

import com.nostratech.project.persistence.domain.Author;
import com.nostratech.project.util.ExtendedSpringBeanUtil;
import com.nostratech.project.vo.AuthorVO;
import com.nostratech.project.vo.CreateAuthorVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by rennytanuwijaya on 4/20/16.
 */
@Component
public class AuthorVOConverter extends BaseVoConverter<AuthorVO, AuthorVO, Author> implements IBaseVoConverter<AuthorVO, AuthorVO, Author> {

    @Autowired
    BookVOConverter bookVOConverter;

    public Author transferVOToModel(CreateAuthorVO vo, Author model) {
        if (null == model) model = new Author();
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"name","gender"});
        
        return model;
    }

    @Override
    public AuthorVO transferModelToVO(Author model, AuthorVO vo) {
        if (null == vo) vo = new AuthorVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"name","gender"});
//        vo.setBookVOList(bookVOConverter.transferListOfModelToListOfVO(model.getBooks(),null));
        return vo;
    }

}
