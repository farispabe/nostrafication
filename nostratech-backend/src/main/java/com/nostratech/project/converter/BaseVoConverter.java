package com.nostratech.project.converter;

import com.nostratech.project.util.ExtendedSpringBeanUtil;

import java.util.ArrayList;
import java.util.Collection;

/**
 * agus w on 3/2/16.
 */
public abstract class BaseVoConverter<Z, V, T> implements IBaseVoConverter<Z, V, T>{

    /**
     * transfer value from vo object to domain object
     * for enum value, please do manually using Enum.values()[ordinal]
     *
     * @param vo
     * @param model
     * @return
     */
    @Override
    public T transferVOToModel(Z vo, T model) {

        // Generated values should not be modified explicitly
        /*ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"id", "creationDate", "createdBy", "modificationDate", "modifiedBy", "version", "active"},
                new String[]{"id", "creationDate", "createdBy", "modificationDate", "modifiedBy", "version", "active"});*/
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"deleted"},
                new String[]{"deleted"});

        return model;
    }

    /**
     * transfer value from list of domain object to list of vo object
     *
     * @param models
     * @param vos
     * @return
     */
    @Override
    public Collection<V> transferListOfModelToListOfVO(Collection<T> models, Collection<V> vos) {
        if (null == vos) vos = new ArrayList<>();
        for (T model : models) {
            V vo = transferModelToVO(model, null);
            vos.add(vo);
        }
        return vos;
    }

    /**
     * transfer value from domain object to vo object
     *
     * @param model
     * @param vo
     * @return
     */
    @Override
    public V transferModelToVO(T model, V vo) {
        ExtendedSpringBeanUtil.copySpecificProperties(model,vo,
                new String[]{"id", "secureId", "creationDate", "createdBy", "modificationDate", "modifiedBy", "deleted","version"},
                new String[]{"internalId", "id", "creationDate", "createdBy", "modificationDate", "modifiedBy", "deleted","version"});
        return vo;
    }
}
