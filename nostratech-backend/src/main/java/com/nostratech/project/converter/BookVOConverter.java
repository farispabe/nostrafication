package com.nostratech.project.converter;

import com.nostratech.project.persistence.domain.Book;
import com.nostratech.project.util.ExtendedSpringBeanUtil;
import com.nostratech.project.vo.BookVO;
import com.nostratech.project.vo.CreateBookVO;
import org.springframework.stereotype.Component;

/**
 * Created by rennytanuwijaya on 4/20/16.
 */
@Component
public class BookVOConverter extends BaseVoConverter<BookVO, BookVO, Book> implements IBaseVoConverter<BookVO, BookVO, Book> {

    public Book transferVOToModel(CreateBookVO vo, Book model) {
        if (null == model) model = new Book();
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"name"});
        return model;
    }

    @Override
    public BookVO transferModelToVO(Book model, BookVO vo) {
        if (null == vo) vo = new BookVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"name"});
        vo.setAuthor(model.getAuthor().getName());
        return vo;
    }

}
