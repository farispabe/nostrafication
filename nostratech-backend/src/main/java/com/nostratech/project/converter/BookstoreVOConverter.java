package com.nostratech.project.converter;

import com.nostratech.project.persistence.domain.Bookstore;
import com.nostratech.project.util.ExtendedSpringBeanUtil;
import com.nostratech.project.vo.BookstoreVO;
import com.nostratech.project.vo.CreateBookstoreVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by rennytanuwijaya on 4/20/16.
 */
@Component
public class BookstoreVOConverter extends BaseVoConverter<BookstoreVO, BookstoreVO, Bookstore> implements IBaseVoConverter<BookstoreVO, BookstoreVO, Bookstore> {

    @Autowired
    BookVOConverter bookVOConverter;

    public Bookstore transferVOToModel(CreateBookstoreVO vo, Bookstore model) {
        if (null == model) model = new Bookstore();
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"name","address"});
        return model;
    }

    @Override
    public BookstoreVO transferModelToVO(Bookstore model, BookstoreVO vo) {
        if (null == vo) vo = new BookstoreVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"name","address"});
        vo.setBookVOList(bookVOConverter.transferListOfModelToListOfVO(model.getBooks(),null));
        return vo;
    }

}
