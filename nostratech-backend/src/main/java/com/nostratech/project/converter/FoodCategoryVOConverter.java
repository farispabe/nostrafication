package com.nostratech.project.converter;

import com.nostratech.project.persistence.domain.Book;
import com.nostratech.project.persistence.domain.FoodCategory;
import com.nostratech.project.util.ExtendedSpringBeanUtil;
import com.nostratech.project.vo.BookVO;
import com.nostratech.project.vo.CreateBookVO;
import com.nostratech.project.vo.FoodCategoryVO;
import org.springframework.stereotype.Component;

/**
 * Created by rennytanuwijaya on 4/20/16.
 */
@Component
public class FoodCategoryVOConverter extends BaseVoConverter<FoodCategoryVO, FoodCategoryVO, FoodCategory>{


    @Override
    public FoodCategory transferVOToModel(FoodCategoryVO vo, FoodCategory model) {
        if (null == model) model = new FoodCategory();
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"nameCategory"},
                new String[]{"name"});
        return model;
    }
}
