package com.nostratech.project.converter;

import com.nostratech.project.persistence.domain.Group;
import com.nostratech.project.persistence.domain.Privilege;
import com.nostratech.project.persistence.repository.AccessLevelRepository;
import com.nostratech.project.vo.GroupDetailVO;
import com.nostratech.project.vo.PrivilegeVO;
import com.nostratech.project.vo.MasterNotificationVO;
import com.nostratech.project.util.ExtendedSpringBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Renny on 8/19/16.
 */

@Component
public class GroupDetailVOConverter extends BaseVoConverter<GroupDetailVO, GroupDetailVO, Group> implements IBaseVoConverter<GroupDetailVO, GroupDetailVO, Group> {


    @Autowired
    PrivilegeVOConverter privilegeVOConverter;

    @Autowired
    AccessLevelRepository accessLevelRepository;

    @Override
    public Group transferVOToModel(GroupDetailVO vo, Group model) {

        if (null == model) model = new Group();

        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"name"});
        if(vo.getAccessLevel() != null)model.setAccessLevel(accessLevelRepository.findByValue(vo.getAccessLevel()));
        if(vo.getId() != null)model.setSecureId(vo.getId());
        return model;
    }

    @Override
    public GroupDetailVO transferModelToVO(Group model, GroupDetailVO vo) {
        if (null == vo) vo = new GroupDetailVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"name"});
        vo.setAccessLevel(model.getAccessLevel().getValue());
        if(model.getSecureId() != null)vo.setId(model.getSecureId());
        List<PrivilegeVO> privilegeVOList = new ArrayList<>();
        List<Privilege> privilegeList = (List) model.getPrivileges();
        privilegeVOConverter.transferListOfModelToListOfVO(privilegeList,privilegeVOList,true);
        vo.setPrivilegeList(privilegeVOList);
        return vo;
    }

}
