package com.nostratech.project.converter;

import com.nostratech.project.persistence.domain.AccessLevel;
import com.nostratech.project.persistence.domain.Group;
import com.nostratech.project.persistence.repository.AccessLevelRepository;
import com.nostratech.project.vo.GroupListVO;
import com.nostratech.project.util.ExtendedSpringBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Renny on 8/22/16.
 */
@Component
public class GroupListVOConverter extends BaseVoConverter<GroupListVO, GroupListVO, Group> implements IBaseVoConverter<GroupListVO, GroupListVO, Group> {

    @Autowired
    AccessLevelRepository accessLevelRepository;

    @Override
    public Group transferVOToModel(GroupListVO vo, Group model) {

        if (null == model) model = new Group();

        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"name"});
        if(vo.getAccessLevel() != null)model.setAccessLevel(accessLevelRepository.findByValue(vo.getAccessLevel()));
        if(vo.getId() != null)model.setSecureId(vo.getId());
        return model;
    }

    @Override
    public GroupListVO transferModelToVO(Group model, GroupListVO vo) {
        if (null == vo) vo = new GroupListVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"name"});
        AccessLevel accessLevel = accessLevelRepository.findByValue(model.getAccessLevel().getValue());
        vo.setAccessLevel(accessLevel.getKeyName());
        return vo;
    }

}
