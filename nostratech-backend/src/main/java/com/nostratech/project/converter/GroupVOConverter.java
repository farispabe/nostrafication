package com.nostratech.project.converter;

import com.nostratech.project.persistence.domain.AccessLevel;
import com.nostratech.project.persistence.repository.AccessLevelRepository;
import com.nostratech.project.vo.GroupSignInVO;
import com.nostratech.project.vo.PrivilegeNameVO;
import com.nostratech.project.vo.PrivilegeVO;
import com.nostratech.project.persistence.domain.Group;
import com.nostratech.project.persistence.domain.Privilege;
import com.nostratech.project.util.ExtendedSpringBeanUtil;
import com.nostratech.project.vo.GroupVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rennytanuwijaya on 4/20/16.
 */
@Component
public class GroupVOConverter extends BaseVoConverter<GroupVO, GroupVO, Group> implements IBaseVoConverter<GroupVO, GroupVO, Group> {

    @Autowired
    AccessLevelRepository accessLevelRepository;

    @Autowired
    PrivilegeVOConverter privilegeVOConverter;

    @Override
    public Group transferVOToModel(GroupVO vo, Group model) {

        if (null == model) model = new Group();
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"name"});
        if(vo.getAccessLevel() != null){
            model.setAccessLevel(accessLevelRepository.findByValue(vo.getAccessLevel()));
        }
        if(vo.getId() != null)model.setSecureId(vo.getId());
        return model;
    }

    @Override
    public GroupVO transferModelToVO(Group model, GroupVO vo) {
        if (null == vo) vo = new GroupVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"name"});
        vo.setAccessLevel(model.getAccessLevel().getValue());
        List<PrivilegeVO> privilegeVOList = new ArrayList<>();
        List<Privilege> privilegeList = (List) model.getPrivileges();
        privilegeVOConverter.transferListOfModelToListOfVO(privilegeList,privilegeVOList,true);
        vo.setPrivilegeList(privilegeVOList);
        return vo;
    }

    public GroupSignInVO transferModelToVOOnly(Group model, GroupSignInVO vo) {
        if (null == vo) vo = new GroupSignInVO();
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"name"});
        vo.setId(model.getSecureId());
        vo.setAccessLevel(model.getAccessLevel().getValue());
        List<PrivilegeNameVO> privilegeVOList = new ArrayList<>();
        List<Privilege> privilegeList = (List) model.getPrivileges();
        privilegeVOConverter.transferListOfModelToListOfVOOnly(privilegeList,privilegeVOList);
        vo.setPrivilegeList(privilegeVOList);
        return vo;
    }
}
