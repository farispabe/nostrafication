package com.nostratech.project.converter;

import com.nostratech.project.persistence.domain.Group;
import com.nostratech.project.persistence.domain.User;
import com.nostratech.project.vo.GroupVO;
import com.nostratech.project.vo.DetailUserVO;
import com.nostratech.project.vo.UserVO;
import com.nostratech.project.util.ExtendedSpringBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Renny on 4/29/16.
 */
@Component
public class UserVoConverter extends BaseVoConverter<UserVO, UserVO, User> implements IBaseVoConverter<UserVO, UserVO, User> {

    @Autowired
    GroupVOConverter groupVOConverter;

    @Override
    public User transferVOToModel(UserVO vo, User model) {
        if (null == model) model = new User();
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"email","active","username","immediateChangePassword","lastUpdatedDate"});
        if(vo.getGroup() != null){
            Group group = new Group();
            groupVOConverter.transferVOToModel(vo.getGroup(),group);
            model.setGroup(group);
        }
        return model;
    }

    @Override
    public UserVO transferModelToVO(User model, UserVO vo) {

        if (null == vo) vo = new UserVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"email","active","username","immediateChangePassword","lastUpdatedDate"});
        if(model.getSecureId() != null)vo.setId(model.getSecureId());
        if(model.getGroup() != null){
            GroupVO group = new GroupVO();
            groupVOConverter.transferModelToVO(model.getGroup(),group);
            vo.setGroup(group);
        }
        return vo;
    }

    public DetailUserVO transferModelToVONew(User model, DetailUserVO vo) {
        if (null == vo) vo = new DetailUserVO();

        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"username","active","groupId","email"});
        vo.setId(model.getSecureId());
        vo.setGroupId(model.getGroup().getSecureId());
        return vo;
    }



}
