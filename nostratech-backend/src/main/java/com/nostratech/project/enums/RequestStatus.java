package com.nostratech.project.enums;

import com.nostratech.project.util.Constants;

/**
 * Created by antin on 12/21/15.
 */
public enum RequestStatus implements BaseModelEnum<String> {
    PENDING(Constants.RequestStatus.PENDING),
    APPROVED(Constants.RequestStatus.APPROVED),
    REJECTED(Constants.RequestStatus.REJECTED);

    private String internalValue;

    RequestStatus(String internalValue) {
        this.internalValue = internalValue;
    }

    @Override
    public String getInternalValue() {
        return internalValue;
    }
}
