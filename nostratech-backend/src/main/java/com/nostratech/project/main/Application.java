package com.nostratech.project.main;

import com.nostratech.project.filter.CORSFilter;
import com.nostratech.project.filter.IpFilter;
import com.nostratech.project.security.SpringSecurityAuditorAware;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.velocity.VelocityAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.servlet.Filter;
import java.util.List;

@Configuration
@EnableJpaAuditing
@ImportResource({
        "file:${ROOT_APP}/configuration/applicationContext.xml",
        "file:${ROOT_APP}/configuration/applicationContextSecurity.xml"
})
@EnableAspectJAutoProxy
@EnableAutoConfiguration(exclude = {
        DataSourceAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class,
        VelocityAutoConfiguration.class})
public class Application {

    @Bean
    public Filter getCorsFilter() {
        return new CORSFilter();
    }

    @Bean
    public Filter getIpFilter(@Value("#{'${ip.whitelist}'.split(',')}") final List<String> allowedIp){
        return new IpFilter(allowedIp);
    }

    @Bean
    public AuditorAware<String> getAuditorAware(){
        return new SpringSecurityAuditorAware();
    }

    public static void main(String[] args) {
        SpringApplication.run(new Object[]{Application.class}, args);
    }

}
