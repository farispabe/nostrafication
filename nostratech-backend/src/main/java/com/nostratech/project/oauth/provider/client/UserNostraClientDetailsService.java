package com.nostratech.project.oauth.provider.client;
	
import com.nostratech.project.oauth.provider.NostraClientDetailService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.provider.ClientDetails;


public class UserNostraClientDetailsService implements UserDetailsService {

	private NostraClientDetailService nostraClientDetailService;

	public UserNostraClientDetailsService(NostraClientDetailService nostraClientDetailService) {
		this.nostraClientDetailService = nostraClientDetailService;
	}

	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		ClientDetails clientDetails = nostraClientDetailService.loadClientByClientId(username);
		String clientSecret = clientDetails.getClientSecret() == null ? "" : clientDetails.getClientSecret();
		return new User(username, clientSecret, clientDetails.getAuthorities());	}

}
