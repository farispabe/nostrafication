package com.nostratech.project.oauth.token.store;

import com.nostratech.project.persistence.domain.User;
import com.nostratech.project.persistence.domain.Parameter;
import com.nostratech.project.persistence.repository.*;
import com.nostratech.project.oauth.provider.UserPasswordAuthenticationToken;
import com.nostratech.project.persistence.domain.AccessToken;
import com.nostratech.project.util.Constants;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;

/**
 * agus w
 */
public class NostraTokenStore implements TokenStore {

    private static final Logger LOGGER = LoggerFactory.getLogger(NostraTokenStore.class);

    private String clientIdAdmin;
    private String clientIdUser;
    private String clientRoleAdmin;
    private String clientRoleUser;

    public String getClientRoleAdmin() {
        return clientRoleAdmin;
    }

    public void setClientRoleAdmin(String clientRoleAdmin) {
        this.clientRoleAdmin = clientRoleAdmin;
    }

    public String getClientRoleUser() {
        return clientRoleUser;
    }

    public void setClientRoleUser(String clientRoleUser) {
        this.clientRoleUser = clientRoleUser;
    }

    private UserRepository userRepository;

    private ParameterRepository parameterRepository;

    private AccessTokenRepository accessTokenRepository;

    private ClientDetailsService clientDetailsService;

    public void setClientIdAdmin(String clientIdAdmin) {
        this.clientIdAdmin = clientIdAdmin;
    }

    public void setClientIdUser(String clientIdUser) {
        this.clientIdUser = clientIdUser;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void setParameterRepository(ParameterRepository parameterRepository) {
        this.parameterRepository = parameterRepository;
    }

    public void setAccessTokenRepository(AccessTokenRepository accessTokenRepository) {
        this.accessTokenRepository = accessTokenRepository;
    }

    public void setClientDetailsService(ClientDetailsService clientDetailsService) {
        this.clientDetailsService = clientDetailsService;
    }

    @Override
    public OAuth2Authentication readAuthentication(OAuth2AccessToken token) {

        LOGGER.debug("readAuthentication OAuth2AccessToken called");

        String clientId = accessTokenRepository.findClientIdByAccessToken(token.getValue(),new Date());

        ClientDetails clientDetails = clientDetailsService.loadClientByClientId(clientId);

        OAuth2Request oAuth2Request = new OAuth2Request(null, clientId, clientDetails.getAuthorities(), true, clientDetails.getScope(),
                clientDetails.getResourceIds(), null, null, null);

        String principal = null, credential = null, authorities = null;
        User user = userRepository.findByAccessToken(token.getValue());
        if (null != user) {
            principal = user.getUsername();
            credential = user.getPassword();
        }

        if (clientId.compareTo(clientIdAdmin) == 0) {
            authorities = clientRoleAdmin;
        }else if(clientId.compareTo(clientIdUser) == 0) {
            authorities = clientRoleUser;
        }



        Authentication authentication = new UserPasswordAuthenticationToken(principal, credential, AuthorityUtils.commaSeparatedStringToAuthorityList(authorities));

        return new OAuth2Authentication(oAuth2Request, authentication);
    }

    @Override
    public OAuth2Authentication readAuthentication(String token) {

        LOGGER.debug("readAuthentication String called");

        return null;
    }

    @Transactional
    @Override
    public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {

        LOGGER.debug("storeAccessToken called, store clientId & accessToken in table");

        storeAccessToken(token.getValue(), authentication.getOAuth2Request().getClientId());

    }

    @Transactional
    public void storeAccessToken(String token, String clientId) {

        if (null == token || token.isEmpty()) throw new BadCredentialsException("Token is empty");

        if (null == clientId || clientId.isEmpty()) throw new BadCredentialsException("Client Id is empty");

        AccessToken accessToken = accessTokenRepository.findByAccessToken(token);

        Parameter parameter = parameterRepository.findByCode(Constants.ParamCode.LOGIN_EXPIRED);
        long expiredMillis = Long.valueOf(parameter.getValue());
        Date expireDate = new Date(System.currentTimeMillis() + expiredMillis);

        if (null == accessToken) {
            accessToken = new AccessToken();
            accessToken.setAccessToken(token);
            accessToken.setClientId(clientId);
        } else {
            if (accessToken.getDeleted())
                accessToken.setDeleted(false);
        }
        accessToken.setExpiredTime(expireDate);
        accessTokenRepository.save(accessToken);
    }

    @Override
    public OAuth2AccessToken readAccessToken(String tokenValue) {

        LOGGER.debug("readAccessToken called, check access token from db");

        String clientIdChecker = accessTokenRepository.findClientIdByAccessToken(tokenValue,new Date());

        return null == clientIdChecker ? null : new DefaultOAuth2AccessToken(tokenValue);
    }

    @Override
    public void removeAccessToken(OAuth2AccessToken token) {

        LOGGER.debug("removeAccessToken called");

    }

    @Override
    public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {

        LOGGER.debug("storeRefreshToken called");

    }

    @Override
    public OAuth2RefreshToken readRefreshToken(String tokenValue) {

        LOGGER.debug("readRefreshToken called");

        return null;
    }

    @Override
    public OAuth2Authentication readAuthenticationForRefreshToken(OAuth2RefreshToken token) {

        LOGGER.debug("readAuthenticationForRefreshToken called");

        return null;
    }

    @Override
    public void removeRefreshToken(OAuth2RefreshToken token) {

        LOGGER.debug("removeRefreshToken called");

    }

    @Override
    public void removeAccessTokenUsingRefreshToken(OAuth2RefreshToken refreshToken) {

        LOGGER.debug("removeAccessTokenUsingRefreshToken called");

    }

    @Transactional(readOnly = true)
    @Override
    public OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {

        LOGGER.debug("getAccessToken called");

        String accessToken = null;
        String username = authentication.getPrincipal().toString();
        String clientId = authentication.getOAuth2Request().getClientId();

        if (null != clientId) {

            if (clientId.equals(clientIdUser) || clientId.equals(clientIdAdmin)) {

                LOGGER.debug("Finding Access Token for USER with username " + username + "...");

                accessToken = userRepository.findAccessTokenByEmail(username);


            }

            if (!StringUtils.isEmpty(accessToken)) {
                LOGGER.debug("Access Token for " + username + " is found");

                return new DefaultOAuth2AccessToken(accessToken);
            }
        }

        return null;
    }

    @Override
    public Collection<OAuth2AccessToken> findTokensByClientIdAndUserName(String clientId, String userName) {

        LOGGER.debug("findTokensByClientIdAndUserName called");

        return null;
    }

    @Override
    public Collection<OAuth2AccessToken> findTokensByClientId(String clientId) {

        LOGGER.debug("findTokensByClientId called");

        return null;
    }
}
