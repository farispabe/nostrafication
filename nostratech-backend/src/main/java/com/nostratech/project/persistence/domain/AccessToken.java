package com.nostratech.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.Date;

/**
 * agus w
 */
@Entity
@Table(name = "ACCESS_TOKENS",
        indexes = {
                @Index(columnList = "ACCESS_TOKEN", name = "IDX_ACCESS_TOKEN", unique = true)
        }
)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region="project")
@DynamicUpdate
@Data
public class AccessToken extends Base {

    @Column(name = "CLIENT_ID", length = 30)
    private String clientId;

    @Column(name = "ACCESS_TOKEN")
    private String accessToken;

    @Column(name = "EXPIRED_TIME")
    private Date expiredTime;

}
