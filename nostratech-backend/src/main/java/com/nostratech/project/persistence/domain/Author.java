package com.nostratech.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by rennytanuwijaya on 4/12/16.
 */
@Entity
@Table(name = "AUTHOR")
@DynamicUpdate
@Data
public class Author extends Base {

    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    @Column(name = "GENDER", nullable = false, length = 100)
    private String gender;

//    @OneToMany(fetch= FetchType.LAZY, mappedBy = "author")
//    private Collection<Book> books;


//    public String toString(){
//        return "Name     : " + name + "\n" +
//                "Gender     : " + gender;
//    }

}
