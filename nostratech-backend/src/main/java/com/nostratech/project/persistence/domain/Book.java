package com.nostratech.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by rennytanuwijaya on 4/12/16.
 */
@Entity
@Table(name = "BOOK")
@DynamicUpdate
@Data
public class Book extends Base {

    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    @ManyToOne
    @JoinColumn(name = "AUTHOR_ID")
    private Author author;
}
