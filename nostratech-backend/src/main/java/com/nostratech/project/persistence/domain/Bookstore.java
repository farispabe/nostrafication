package com.nostratech.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by rennytanuwijaya on 4/12/16.
 */
@Entity
@Table(name = "BOOKSTORE")
@DynamicUpdate
@Data
public class Bookstore extends Base {

    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    @Column(name = "ADDRESS", nullable = false, length = 100)
    private String address;

    @ManyToMany(fetch = FetchType.EAGER, targetEntity = Book.class)
    @JoinTable(
            name = "BOOKSTORE_BOOK",indexes = {
            @Index(columnList = "BOOKSTORE_ID,BOOK_ID", name = "UK_BOOKSTORE_BOOK", unique = true)},
            joinColumns = {@JoinColumn(name = "BOOKSTORE_ID")},
            inverseJoinColumns = {@JoinColumn(name = "BOOK_ID")})
    private Collection<Book> books;
}
