package com.nostratech.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * Created by rennytanuwijaya on 4/12/16.
 */
@Entity
@Table(name = "CATEGORY")
@DynamicUpdate
@Data
public class Category extends Base {

    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    @Column(name = "DESCRIPTION", length = 30)
    private String description;
}
