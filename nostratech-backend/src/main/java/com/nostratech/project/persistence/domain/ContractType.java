package com.nostratech.project.persistence.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "CONTRACT_TYPE")
@Data
public class ContractType extends NewBase {

    private Date createdAt;
}
