package com.nostratech.project.persistence.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Renny on 04/02/18.
 */

@Entity
@Table(name = "FOOD_CATEGORY")
@Data
public class FoodCategory extends Base{

    @Column(name = "NAME")
    private String name;

}
