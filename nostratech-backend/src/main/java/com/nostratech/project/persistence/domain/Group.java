package com.nostratech.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.util.Collection;

/**
 * Created by rennytanuwijaya on 4/12/16.
 */
@Entity
@Table(name = "GROUPS",
        indexes = {
                @Index(columnList = "NAME", name = "UK_NAME", unique = true)
        }
)
@Data
public class Group extends Base {

    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    @ManyToOne
    @JoinColumn(name="ACCESS_LEVEL")
    private AccessLevel accessLevel;

    @ManyToMany(fetch = FetchType.EAGER,targetEntity = Privilege.class)
    @JoinTable(
            name = "GROUP_PRIVILEGE",indexes = {
            @Index(columnList = "GROUP_ID,PRIVILEGE_ID", name = "UK_GROUP_PRIVILEGE", unique = true)},
            joinColumns = {@JoinColumn(name = "GROUP_ID")},
            inverseJoinColumns = {@JoinColumn(name = "PRIVILEGE_ID")})
    private Collection<Privilege> privileges;
}
