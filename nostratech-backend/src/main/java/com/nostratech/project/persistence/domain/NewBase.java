package com.nostratech.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)    // Must be temporarily commented when unit testing with Junit (otherwise Exception: sun.reflect.annotation.TypeNotPresentExceptionProxy)
@DynamicUpdate
@Data
public abstract class NewBase implements Serializable {

    private static final long serialVersionUID = -7369920601847524273L;

    public NewBase() {
    }

    @Id
    @GeneratedValue
    @Column(name = "ID")
    protected Integer id;

    /**
     * Random id for security reason
     */
    @Column(name = "SECURE_ID", unique = true, length = 36)
    private String secureId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_CREATED")
    private Date creationDate;

    @Column(name = "CREATED_BY", length = 30)
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_MODIFIED")
    private Date modificationDate;

    @Column(name = "MODIFIED_BY", length = 30)
    private String modifiedBy;

    @Column(name = "VERSION")
    private Integer version;

    @Column(name = "DELETED")
    private Boolean deleted;
}
