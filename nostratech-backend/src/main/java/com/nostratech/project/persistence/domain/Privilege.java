package com.nostratech.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * Created by rennytanuwijaya on 4/12/16.
 */
@Entity
@Table(name = "PRIVILEGE",
        indexes = {
                @Index(columnList = "NAME", name = "UK_NAME", unique = true)
        }
)
@DynamicUpdate
@Data
public class Privilege extends Base {

    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    @Column(name = "CATEGORY", length = 20)
    private String category;

    @Column(name = "DESCRIPTION", length = 30)
    private String description;
}
