package com.nostratech.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * Created by rennytanuwijaya on 4/11/16.
 */
@Entity
@Table(name = "PROFILE_ADMIN",
        indexes = {
                @Index(columnList = "USER_ID", name = "UK_USER_ID", unique = true)
        }
)
@DynamicUpdate
@Data
public class ProfileAdmin extends Base {


    @OneToOne
    @JoinColumn(name="USER_ID", nullable = false)
    private User user;

    @Column(name = "FULL_NAME", nullable = false, length = 255)
    private String fullName;

}
