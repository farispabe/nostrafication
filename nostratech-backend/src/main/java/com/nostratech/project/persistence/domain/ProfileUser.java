package com.nostratech.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
/**
 * Created by rennytanuwijaya on 4/11/16.
 */
@Entity
@Table(name = "PROFILE_USER",
        indexes = {
                @Index(columnList = "USER_ID", name = "UK_USER_ID", unique = true)
        }
)
@DynamicUpdate
@Data
public class ProfileUser extends Base {

    @OneToOne
    @JoinColumn(name="USER_ID",nullable = false)
    private User user;

    @Column(name = "FULL_NAME", nullable = false, length = 255)
    private String fullName;

}
