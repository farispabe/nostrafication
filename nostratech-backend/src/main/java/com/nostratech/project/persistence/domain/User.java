package com.nostratech.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 * agus w
 */

@Entity
@Table(name = "USER_INTERNAL",
        indexes = {
                @Index(columnList = "EMAIL", name = "UK_EMAIL", unique = true),
                @Index(columnList = "USERNAME", name = "UK_USERNAME", unique = true),
                @Index(columnList = "ACCESS_TOKEN", name = "UK_ACCESS_TOKEN", unique = true),
                @Index(columnList = "EMAIL,ACCESS_TOKEN", name = "IDX_EMAIL_ACCESS_TOKEN")
        },
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"EMAIL","ACCESS_TOKEN"}, name = "UK_USER")
        }
)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region="project")
@DynamicUpdate
@Data
public class User extends Base {

    @Column(name = "EMAIL", nullable = false, length = 50)
    private String email;

    @Column(name = "PASSWORD", length = 128, nullable = false)
    private String password;

    @Column(name = "CLIENT_ID", length = 30)
    private String clientId;

    @Column(name = "ACTIVE")
    private Boolean active;

    @Column(name = "USERNAME", length = 100)
    private String username ;

    @Column(name = "ACCESS_TOKEN", length = 36)
    private String accessToken;

    @Column(name = "IMMEDIATE_CHANGE_PASSWORD", length = 36)
    private Boolean immediateChangePassword;

    @ManyToOne
    @JoinColumn(name = "GROUPS")
    private Group group;

    @Column(name = "LAST_UPDATED_PASSWORD", length = 36)
    private Date lastUpdatedPassword;

    @PrePersist
    public void prePersist() {
        super.prePersist();
        this.active = true;
        this.immediateChangePassword = true;
        this.lastUpdatedPassword = new Date();
        if(null != this.email)
            this.accessToken = UUID.nameUUIDFromBytes(this.email.concat(this.getClass().getName()).getBytes()).toString();
    }


}
