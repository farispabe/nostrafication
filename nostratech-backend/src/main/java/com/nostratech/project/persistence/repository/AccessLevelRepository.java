package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.AccessLevel;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Created by rennytanuwijaya on 4/20/16.
 */
@Repository
public interface AccessLevelRepository  extends BaseRepository<AccessLevel> {
    public AccessLevel findBySecureId(String secureId);
    public AccessLevel findByValue(String value);
    public List<AccessLevel> findByValueGreaterThanEqual(String value);
}
