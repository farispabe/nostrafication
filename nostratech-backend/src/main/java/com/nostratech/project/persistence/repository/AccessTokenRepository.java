package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.AccessToken;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface AccessTokenRepository extends BaseRepository<AccessToken> {

    @Query("SELECT u.clientId FROM AccessToken u WHERE u.accessToken =:accessToken AND u.deleted = false AND u.expiredTime >= :now")
    public String findClientIdByAccessToken(@Param("accessToken") String accessToken, @Param("now") Date now);

    public AccessToken findByAccessToken(String accessToken);
}
