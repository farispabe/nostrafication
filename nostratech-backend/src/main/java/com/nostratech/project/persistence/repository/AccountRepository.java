package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.Account;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<Account, Integer>{
    @Query("SELECT a FROM Account a")
    List<Account> findAll();
}
