package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.Author;
import com.nostratech.project.persistence.domain.Bookstore;
import org.springframework.stereotype.Repository;

/**
 * Created by rennytanuwijaya on 4/12/16.
 */
@Repository
public interface AuthorRepository extends BaseRepository<Author> {

}
