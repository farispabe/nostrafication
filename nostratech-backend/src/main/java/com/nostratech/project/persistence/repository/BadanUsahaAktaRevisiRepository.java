package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.BadanUsahaAktaRevisi;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BadanUsahaAktaRevisiRepository extends CrudRepository<BadanUsahaAktaRevisi, Integer> {
    @Query("SELECT buar FROM BadanUsahaAktaRevisi buar")
    List<BadanUsahaAktaRevisi> findAll();
}
