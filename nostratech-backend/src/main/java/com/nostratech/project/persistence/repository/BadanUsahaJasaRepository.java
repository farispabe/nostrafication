package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.BadanUsahaJasa;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BadanUsahaJasaRepository extends CrudRepository<BadanUsahaJasa, Integer> {
    @Query("SELECT buj FROM BadanUsahaJasa buj")
    List<BadanUsahaJasa> findAll();
}
