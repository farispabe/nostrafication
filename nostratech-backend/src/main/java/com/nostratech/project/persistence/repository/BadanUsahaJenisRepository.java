package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.BadanUsahaJenis;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BadanUsahaJenisRepository extends CrudRepository<BadanUsahaJenis, Integer>{
    @Query("SELECT buje FROM BadanUsahaJenis buje")
    List<BadanUsahaJenis> findAll();
}
