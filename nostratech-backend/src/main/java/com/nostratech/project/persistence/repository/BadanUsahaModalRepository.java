package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.BadanUsahaModal;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BadanUsahaModalRepository extends CrudRepository<BadanUsahaModal, Integer> {
    @Query("SELECT bum FROM BadanUsahaModal bum")
    List<BadanUsahaModal> findAll();
}
