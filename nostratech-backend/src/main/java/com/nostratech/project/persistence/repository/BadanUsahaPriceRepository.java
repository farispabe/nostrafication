package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.BadanUsahaPrice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BadanUsahaPriceRepository extends CrudRepository<BadanUsahaPrice, Integer> {
    @Query("SELECT bup FROM BadanUsahaPrice bup")
    List<BadanUsahaPrice> findAll();
}
