package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.BadanUsaha;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BadanUsahaRepository extends CrudRepository<BadanUsaha, Integer> {
    @Query("SELECT bu FROM BadanUsaha bu")
    List<BadanUsaha> findAll();
}
