package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.BadanUsahaUpload;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BadanUsahaUploadRepository extends CrudRepository<BadanUsahaUpload, Integer> {
    @Query("SELECT buu FROM BadanUsahaUpload buu")
    List<BadanUsahaUpload> findAll();
}
