package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.Book;
import com.nostratech.project.persistence.domain.Privilege;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by rennytanuwijaya on 4/12/16.
 */
@Repository
public interface BookRepository extends BaseRepository<Book> {

}
