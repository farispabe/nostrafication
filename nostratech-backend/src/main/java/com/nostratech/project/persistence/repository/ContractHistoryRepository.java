package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.ContractHistory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractHistoryRepository extends CrudRepository<ContractHistory, Integer>{
    @Query("SELECT ch FROM ContractHistory ch")
    List<ContractHistory> findAll();
}
