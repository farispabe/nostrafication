package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.Contract;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractRepository extends CrudRepository<Contract, Integer> {
    @Query("SELECT c FROM Contract c")
    List<Contract> findAll();
}
