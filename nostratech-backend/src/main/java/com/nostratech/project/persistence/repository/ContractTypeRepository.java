package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.ContractType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractTypeRepository extends CrudRepository<ContractType, Integer> {
    @Query("SELECT ct FROM ContractType ct")
    List<ContractType> findAll();
}
