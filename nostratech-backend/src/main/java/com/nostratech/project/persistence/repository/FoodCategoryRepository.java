package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.FoodCategory;
import org.springframework.stereotype.Repository;

/**
 * Created by rennytanuwijaya on 4/12/16.
 */
@Repository
public interface FoodCategoryRepository extends BaseRepository<FoodCategory> {
}
