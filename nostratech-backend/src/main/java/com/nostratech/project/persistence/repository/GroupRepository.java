package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.Group;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by rennytanuwijaya on 4/12/16.
 */
@Repository
public interface GroupRepository extends BaseRepository<Group> {

    public List<Group> findByAccessLevel(String accessLevel);

    public List<Group> findByAccessLevelGreaterThanEqual(int accessLevel);

    public Group findBySecureId(String secureId);

    public Group findById(Integer id);

    public Group findByName(String name);

    public Page<Group> findByNameLike(String name, Pageable pageable);

    public Group findByNameLike(String name);


}
