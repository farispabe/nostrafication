package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.IndustryType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IndustryTypeRepository extends CrudRepository<IndustryType, Integer>{
    @Query("SELECT it FROM IndustryType it")
    List<IndustryType> findAll();
}
