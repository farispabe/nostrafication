package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.Invoice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvoiceRepository extends CrudRepository<Invoice, Integer> {
    @Query("SELECT i FROM Invoice i")
    List<Invoice> findAll();
}
