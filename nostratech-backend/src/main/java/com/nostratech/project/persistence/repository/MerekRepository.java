package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.Merek;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MerekRepository extends CrudRepository<Merek, Integer> {
    @Query("SELECT m FROM Merek m")
    List<Merek> findAll();
}
