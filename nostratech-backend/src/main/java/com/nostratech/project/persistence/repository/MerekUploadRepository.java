package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.MerekUpload;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MerekUploadRepository extends CrudRepository<MerekUpload, Integer> {
    @Query("SELECT mu FROM MerekUpload mu")
    List<MerekUpload> findAll();
}
