package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.NostraficationSupplier;
import org.springframework.stereotype.Repository;

@Repository
public interface NostraficationSupplierRepository extends BaseRepository<NostraficationSupplier> {
}
