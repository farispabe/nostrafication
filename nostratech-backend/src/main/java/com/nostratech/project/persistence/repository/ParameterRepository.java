package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.Parameter;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by antin on 12/22/15.
 */
@Repository
public interface ParameterRepository extends BaseRepository<Parameter> {

    @Query("SELECT p FROM Parameter p WHERE p.code =:code AND p.deleted = false ")
    public Parameter findByCode(@Param("code") String code);

}
