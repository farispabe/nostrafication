package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.Payment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentRepository extends CrudRepository<Payment, Integer> {
    @Query("SELECT p FROM Payment p")
    List<Payment> findAll();
}
