package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.PaymentSetting;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentSettingRepository extends CrudRepository<PaymentSetting, Integer> {
    @Query("SELECT ps FROM PaymentSetting ps")
    List<PaymentSetting> findAll();
}
