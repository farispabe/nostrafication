package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.ProfileAdmin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by rennytanuwijaya on 4/11/16.
 */

@Repository
public interface ProfileAdminRepository extends BaseRepository<ProfileAdmin> {

    ProfileAdmin findByUserId(Integer userId);

}
