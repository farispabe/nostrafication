package com.nostratech.project.persistence.repository;
import com.nostratech.project.persistence.domain.ProfileUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by rennytanuwijaya on 4/11/16.
 */
public interface ProfileUserRepository extends BaseRepository<ProfileUser> {

    public ProfileUser findByUserId(int userId);

}
