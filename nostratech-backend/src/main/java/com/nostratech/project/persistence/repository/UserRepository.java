package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.Group;
import com.nostratech.project.persistence.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface UserRepository extends BaseRepository<User> {

    @Query("SELECT u FROM User u WHERE u.email =:email")
    public User findByEmail(@Param("email")String email);

    @Query("SELECT u.accessToken FROM User u WHERE u.email =:email AND u.deleted = false")
    public String findAccessTokenByEmail(@Param("email") String email);

    @Query("SELECT u FROM User u WHERE u.accessToken =:accessToken AND u.deleted = false")
    public User findByAccessToken(@Param("accessToken")String accessToken);

    @Query("SELECT u FROM User u WHERE u.username = :username AND u.deleted = false")
    public User findByUsername(@Param("username") String username);

    public User findByUsernameOrEmail(String username,String email);

    public List<User> findByGroup(Group group);

    public Page<User> findByIdInAndEmailLikeAndUsernameLike(Collection<Integer> id,String email, String usernmae, Pageable pageable);
}
