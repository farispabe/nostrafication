package com.nostratech.project.persistence.repository;

import com.nostratech.project.persistence.domain.Voucher;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VoucherRepository extends CrudRepository<Voucher, Integer> {
    @Query("SELECT v FROM Voucher v")
    List<Voucher> findAll();
}
