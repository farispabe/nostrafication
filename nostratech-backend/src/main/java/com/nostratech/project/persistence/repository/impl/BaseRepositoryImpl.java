package com.nostratech.project.persistence.repository.impl;

import com.nostratech.project.persistence.domain.Base;
import com.nostratech.project.persistence.repository.BaseRepository;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by fani on 5/2/15.
 * Edited by agus w
 */

public class BaseRepositoryImpl<T extends Base> extends SimpleJpaRepository<T, Integer> implements BaseRepository<T> {

    public BaseRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
    }

    public BaseRepositoryImpl(Class<T> domainClass, EntityManager entityManager) {
        this(JpaEntityInformationSupport.getEntityInformation(domainClass, entityManager), entityManager);
    }

    @Override
    @Transactional
    public <S extends T> S save(S entity) {
        //entity.setSecureId(UUID.randomUUID().toString());
        return super.save(entity);
    }

    @Override
    @Transactional
    public void delete(T entity) {
        if (null != entity) {
            entity.setDeleted(true);
            super.save(entity);
        }
    }

    @Override
    @Transactional
    public void deleteRow(T entity) {
        if (null != entity) {
            super.delete(entity);
        }
    }


    @Override
    @Transactional
    public void deleteRow(Iterable<? extends T> entities) {
        if (null != entities) {
            for (T entity : entities) {
                deleteRow(entity);
            }
        }
    }

    @Override
    @Transactional
    public void delete(Iterable<? extends T> entities) {
        if (null != entities) {
            for (T entity : entities) {
                entity.setDeleted(true);
            }
            super.save(entities);
        }
    }

    @Override
    public T findBySecureId(final String secureId) {
        return findOne(new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.and(
                        criteriaBuilder.equal(root.get("secureId"), secureId),
                        criteriaBuilder.equal(root.get("deleted"), false)
                );
            }
        });
    }

    @Override
    public T findById(Integer id) {
        return findOne(new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.and(
                        criteriaBuilder.equal(root.get("id"), id),
                        criteriaBuilder.equal(root.get("deleted"), false)
                );
            }
        });
    }

    @Override
    public List<T> findAll() {
        return findAll(new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("deleted"), false);
            }
        });
    }

    @Override
    public boolean exists(Integer integer) {
        return findById(integer)!=null;
    }
}
