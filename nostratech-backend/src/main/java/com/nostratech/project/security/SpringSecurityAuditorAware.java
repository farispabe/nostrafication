package com.nostratech.project.security;

import com.nostratech.project.service.SignInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;

/**
 * agus w
 */
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Autowired
    SignInService signInService;

    @Override
    public String getCurrentAuditor() {

        return signInService.getUsername();
    }

}
