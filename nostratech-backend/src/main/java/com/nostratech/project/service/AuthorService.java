package com.nostratech.project.service;

import com.nostratech.project.converter.AuthorVOConverter;
import com.nostratech.project.persistence.domain.Author;
import com.nostratech.project.persistence.domain.Book;
import com.nostratech.project.persistence.repository.AuthorRepository;
import com.nostratech.project.persistence.repository.BookRepository;
import com.nostratech.project.util.ErrorUtil;
import com.nostratech.project.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * Created by rennytanuwijaya on 4/11/16.
 */

@Service
public class AuthorService {

    @Autowired
    AuthorVOConverter authorVOConverter;

    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    BookRepository bookRepository;

    @Transactional
    public BaseVO addAuthor(CreateAuthorVO createAuthorVO){
        //validator
        Author authorAdd = authorVOConverter.transferVOToModel(createAuthorVO,null);
        Author saved = authorRepository.save(authorAdd);
        createAuthorVO.getBookVOList().forEach(bookVO->{
            Book bookAdd = new Book();
            bookAdd.setName(bookVO.getName());
            bookAdd.setAuthor(saved);
            bookRepository.save(bookAdd);
        });
        BaseVO response = new BaseVO();
        response.setId(saved.getSecureId());
        response.setVersion(saved.getVersion());
        return response;

    }
    public Collection<AuthorVO> getListOfAuthor() {
        List<Author> authorList = authorRepository.findAll();
        Collection<AuthorVO> authorVOList = authorVOConverter.transferListOfModelToListOfVO(authorList,null);
        return authorVOList;
    }

    public AuthorVO getDetail(String id) {
        //validator
        Author authorFound = authorRepository.findBySecureId(id);
        if(authorFound == null) throw ErrorUtil.notFoundError("Author");
        AuthorVO authorVO = authorVOConverter.transferModelToVO(authorFound,null);
        return authorVO;
    }
}
