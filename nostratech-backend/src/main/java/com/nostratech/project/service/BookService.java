package com.nostratech.project.service;

import com.nostratech.project.converter.BookVOConverter;
import com.nostratech.project.converter.GroupVOConverter;
import com.nostratech.project.converter.PrivilegeVOConverter;
import com.nostratech.project.exception.NostraException;
import com.nostratech.project.persistence.domain.*;
import com.nostratech.project.persistence.repository.AuthorRepository;
import com.nostratech.project.persistence.repository.BookRepository;
import com.nostratech.project.persistence.repository.PrivilegeRepository;
import com.nostratech.project.persistence.repository.UserRepository;
import com.nostratech.project.util.Constants;
import com.nostratech.project.util.ErrorUtil;
import com.nostratech.project.validator.BookValidator;
import com.nostratech.project.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by rennytanuwijaya on 4/11/16.
 */

@Service
public class BookService {

    @Autowired
    BookVOConverter bookVOConverter;

    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    BookValidator bookValidator;

    @Transactional
    public BaseVO addBook(CreateBookVO createBookVO){
        //TODO : Validate VO
        String msg = bookValidator.validateAddBook(createBookVO);
        if(msg != null) throw new NostraException(msg);

        //TODO : Validate domain yang diperlukan
        Author author = authorRepository.findBySecureId(createBookVO.getAuthorId());
        if(author == null) throw ErrorUtil.notFoundError("Author");


        Book bookAdd = bookVOConverter.transferVOToModel(createBookVO,null);
        bookAdd.setAuthor(author);
        Book saved = bookRepository.save(bookAdd);

        BaseVO response = new BaseVO();
        response.setId(saved.getSecureId());
        response.setVersion(saved.getVersion());

        return response;

    }
    public Collection<BookVO> getListOfBook() {
        List<Book> bookFound = bookRepository.findAll();
        Collection<BookVO> bookVOList = bookVOConverter.transferListOfModelToListOfVO(bookFound,null);
        return bookVOList;
    }

    public BookVO getDetail(String id) {
        //validator
        Book bookFound = bookRepository.findBySecureId(id);
        if(bookFound == null) throw ErrorUtil.notFoundError("Book");
        BookVO bookVO = bookVOConverter.transferModelToVO(bookFound,null);
        return bookVO;
    }
}
