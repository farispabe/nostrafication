package com.nostratech.project.service;

import com.nostratech.project.converter.BookVOConverter;
import com.nostratech.project.converter.BookstoreVOConverter;
import com.nostratech.project.persistence.domain.Author;
import com.nostratech.project.persistence.domain.Book;
import com.nostratech.project.persistence.domain.Bookstore;
import com.nostratech.project.persistence.repository.AuthorRepository;
import com.nostratech.project.persistence.repository.BookRepository;
import com.nostratech.project.persistence.repository.BookstoreRepository;
import com.nostratech.project.util.ErrorUtil;
import com.nostratech.project.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by rennytanuwijaya on 4/11/16.
 */

@Service
public class BookstoreService {

    @Autowired
    BookstoreVOConverter bookstoreVOConverter;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    BookstoreRepository bookstoreRepository;

    public BaseVO addBookStore(CreateBookstoreVO createBookstoreVO){
        //validator
        Bookstore bookstoreAdd = bookstoreVOConverter.transferVOToModel(createBookstoreVO,null);
        Collection<Book> bookAdd = new ArrayList<>();

        createBookstoreVO.getBookIdList().forEach(book ->{
            Book bookFound = bookRepository.findBySecureId(book.getId());
            if(bookFound == null) throw ErrorUtil.notFoundError("Book");
            bookAdd.add(bookFound);
        });

        bookstoreAdd.setBooks(bookAdd);
        Bookstore saved = bookstoreRepository.save(bookstoreAdd);
        BaseVO response = new BaseVO();
        response.setId(saved.getSecureId());
        response.setVersion(saved.getVersion());
        return response;

    }
    public Collection<BookstoreVO> getListOfBookStore() {
        List<Bookstore> bookstoresFound = bookstoreRepository.findAll();
        Collection<BookstoreVO> bookVOList = bookstoreVOConverter.transferListOfModelToListOfVO(bookstoresFound,null);
        return bookVOList;
    }

    public BookstoreVO getDetail(String id) {
        //validator
        Bookstore bookstoreFound = bookstoreRepository.findBySecureId(id);
        if(bookstoreFound == null) throw ErrorUtil.notFoundError("Bookstore");
        BookstoreVO bookstoreVOVO = bookstoreVOConverter.transferModelToVO(bookstoreFound,null);
        return bookstoreVOVO;
    }
}
