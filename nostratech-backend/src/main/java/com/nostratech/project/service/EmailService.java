package com.nostratech.project.service;

import com.nostratech.project.vo.mail.BaseEmailVO;
import com.nostratech.project.vo.mail.EmailVO;
import org.apache.commons.lang.CharEncoding;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class EmailService {

    public static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    @Value(value = "${mail.smtp.host}") String smtpHost;

    @Value(value = "${mail.smtp.port}") String smtpPort;

    @Value(value = "${mail.smtp.auth}") String smtpAuth;

    @Value(value = "${mail.smtp.socketFactory.port}") String factoryPort;

    @Value(value = "${mail.smtp.socketFactory.class}") String factoryClass;

    @Value(value = "${mail.username}") String mailUsername;

    @Value(value = "${mail.password}") String mailPassword;

    @Value(value = "${mail.ssl}") String mailSsl;

    @Autowired
    VelocityEngine velocityEngine;

    public boolean send(EmailVO emailVO) {
        boolean result = true;
        try {
            String subject = emailVO.getSubject();
            String sender = emailVO.getSender();
            String destination = emailVO.getDestination();
            String payload = constructMessage(emailVO);

            MimeMessage message = constructMimeMessage(subject, sender, destination, payload);

            Transport.send(message);
            LOGGER.info("Sent message successfully : " + emailVO.toString());
        } catch (MessagingException mex) {
            LOGGER.error("failed to send message {}", mex.getMessage());
            LOGGER.error(mex.toString());
            result = false;
        }

        return result;
    }

    private String constructMessage(BaseEmailVO emailVO) {
        String message = "Content is not defined yet.";

        if (emailVO.getTemplateLocation() != null && emailVO.getTemplateValues() != null) {
            message = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, emailVO.getTemplateLocation(), CharEncoding.UTF_8, emailVO.getTemplateValues());
        }

        return message;
    }

    private MimeMessage constructMimeMessage(final String subject, final String sender, final String destination, final String payload) throws MessagingException {

        Properties properties = System.getProperties();

        properties.setProperty("mail.smtp.host", smtpHost);
        properties.setProperty("mail.smtp.port", smtpPort);
        properties.setProperty("mail.smtp.auth", smtpAuth);

        Boolean useSsl = Boolean.parseBoolean(mailSsl);


        if(useSsl) {
            properties.setProperty("mail.smtp.socketFactory.port", factoryPort);
            properties.setProperty("mail.smtp.socketFactory.class", factoryClass);
        }

        Session session = Session.getInstance(properties,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(mailUsername, mailPassword);
                    }
                });

        MimeMessage message = new MimeMessage(session);

        if(null != sender && !sender.isEmpty())
            message.setFrom(new InternetAddress(sender));

        if(null != destination && !destination.isEmpty()) {

            if(destination.contains(",")) //"a@gmail.com,"
                message.addRecipients(Message.RecipientType.TO, destination);
            else
                //message.addRecipient(Message.RecipientType.TO, new InternetAddress(destination));
                message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(destination));
        }

        message.setSubject(subject);
        message.setContent(payload, "text/html");

        return message;
    }

/*
    public boolean testSend(){
        boolean result = true;

        try{
            Properties properties = System.getProperties();
            properties.setProperty("mail.smtp.host", "smtp.gmail.com");
            properties.setProperty("mail.smtp.port", "465");
            properties.setProperty("mail.smtp.auth", "true");
            properties.setProperty("mail.smtp.socketFactory.port", "465");
            properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

            Session session = Session.getInstance(properties,
                    new Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication("ums.notification@gmail.com", "insant4ni");
                        }
                    });
            MimeMessage simpleMessage=new MimeMessage(session);
            simpleMessage.setFrom(new InternetAddress("ums.notification@gmail.com", "Test Email Sender"));
            simpleMessage.setRecipient(Message.RecipientType.TO, new InternetAddress("derry@mailinator.com", "derry"));
            simpleMessage.setSubject("Test Email Sender");
            simpleMessage.setText("hello world, this email send from localhost");

            Transport.send(simpleMessage);

        }catch (MessagingException me){
            me.printStackTrace();
            result = false;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            result = false;
        }

        return result;

    }
    */
}
