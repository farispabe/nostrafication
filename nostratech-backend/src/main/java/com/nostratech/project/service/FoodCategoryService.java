package com.nostratech.project.service;

import com.nostratech.project.converter.FoodCategoryVOConverter;
import com.nostratech.project.persistence.domain.FoodCategory;
import com.nostratech.project.persistence.repository.FoodCategoryRepository;
import com.nostratech.project.vo.FoodCategoryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Renny on 04/02/18.
 */

@Service
public class FoodCategoryService {


    @Autowired
    FoodCategoryVOConverter foodCategoryVOConverter;

    @Autowired
    FoodCategoryRepository foodCategoryRepository;

    public Boolean addFoodCategory(FoodCategoryVO foodCategoryVO){
        FoodCategory foodCategoryToAdd =
                foodCategoryVOConverter.transferVOToModel(foodCategoryVO,null);
        foodCategoryRepository.save(foodCategoryToAdd);
        return true;
    }
}
