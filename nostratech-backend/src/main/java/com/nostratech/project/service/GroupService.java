package com.nostratech.project.service;

import com.nostratech.project.converter.GroupDetailVOConverter;
import com.nostratech.project.converter.GroupListVOConverter;
import com.nostratech.project.converter.GroupVOConverter;
import com.nostratech.project.converter.PrivilegeVOConverter;
import com.nostratech.project.persistence.domain.AccessLevel;
import com.nostratech.project.persistence.repository.AccessLevelRepository;
import com.nostratech.project.persistence.domain.User;
import com.nostratech.project.persistence.repository.UserRepository;
import com.nostratech.project.vo.MasterNotificationVO;
import com.nostratech.project.converter.IBaseVoConverter;
import com.nostratech.project.exception.NostraException;
import com.nostratech.project.persistence.domain.Group;
import com.nostratech.project.persistence.domain.Privilege;
import com.nostratech.project.persistence.repository.GroupRepository;
import com.nostratech.project.persistence.repository.PrivilegeRepository;
import com.nostratech.project.persistence.repository.BaseRepository;
import com.nostratech.project.util.Constants;
import com.nostratech.project.util.ErrorUtil;
import com.nostratech.project.vo.*;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

/**
 * Created by rennytanuwijaya on 4/13/16.
 */
@Service
public class GroupService extends BaseVoService<Group, GroupVO, GroupVO> {

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    PrivilegeRepository privilegeRepository;

    @Autowired
    PrivilegeService privilegeService;

    @Autowired
    GroupVOConverter groupVOConverter;

    @Autowired
    GroupDetailVOConverter groupDetailVOConverter;

    @Autowired
    PrivilegeVOConverter privilegeVOConverter;

    @Autowired
    UserRepository userRepository;

    @Autowired
    GroupListVOConverter groupListVOConverter;

    @Autowired
    AccessLevelRepository accessLevelRepository;

    public BaseVO addGroup(GroupVO groupVO) {
        if(!privilegeService.checkPrivilege(Constants.PrivilegeType.CREATE_GROUP)){
            throw ErrorUtil.notEligible("Create Group");
        }
        if(groupVO.getName().isEmpty()){
            throw ErrorUtil.notNullError("Group Name");
        }
        if(null == groupVO.getAccessLevel()){
            throw ErrorUtil.notNullError("Group Access Level");
        }
        Group isExist = groupRepository.findByName(groupVO.getName());
        if(isExist != null){
            throw new NostraException("Group sudah terdaftar");
        }
        Group group = new Group();
        AccessLevel accessLevel = accessLevelRepository.findByValue(groupVO.getAccessLevel());
        group.setAccessLevel(accessLevel);
        group.setName(groupVO.getName());
        if (groupVO.getPrivilegeList() != null){
            Collection privilegesSet = new ArrayList<>();
            groupVO.getPrivilegeList().forEach(item -> {
                Privilege privilege = privilegeRepository.findBySecureId(item.getId());
                if(privilege == null){
                    throw new NostraException("Privilege Not Found ! ");
                }
                privilegesSet.add(privilege);
            });
            group.setPrivileges(privilegesSet);
        }
        Group saved = groupRepository.saveAndFlush(group);
        BaseVO response = new BaseVO();
        response.setId(saved.getSecureId());
        return response;
    }

    public GroupDetailVO getGroup(String secureId) {
        Group group = groupRepository.findBySecureId(secureId);
        if(group == null){
            throw  ErrorUtil.notFoundError("Group");
        }
        GroupDetailVO groupDetailVO = new GroupDetailVO();
        groupDetailVOConverter.transferModelToVO(group,groupDetailVO);
        return groupDetailVO;
    }

    @Transactional
    public Boolean assignPrivilegeToGroup(AssignPrivilegeVO assignPrivilegeVO) {
        if(!privilegeService.checkPrivilege(Constants.PrivilegeType.CREATE_GROUP)){
            throw ErrorUtil.notEligible("Assign Privilege to Group");
        }
        Group group = groupRepository.findBySecureId(assignPrivilegeVO.getGroupId());
        if(null == group){
            throw ErrorUtil.notFoundError("Group");
        }
        assignPrivilegeVO.getPrivilegeList().forEach(item -> {
            Privilege privilege = privilegeRepository.findBySecureId(item.getId());
            if (null == privilege){
                throw ErrorUtil.notFoundError("Privilege");
            }
            group.getPrivileges().add(privilege);
        });
        groupRepository.save(group);
        return Boolean.TRUE;
    }

    public List<GroupListVO> getListOfGroup(String accessLevel) {
        if(!privilegeService.checkPrivilege(Constants.PrivilegeType.READ_GROUP) && !privilegeService.checkPrivilege(Constants.PrivilegeType.CREATE_USER)){
            throw ErrorUtil.notEligible("Melihat group");
        }
        List<Group> group = groupRepository.findByAccessLevel(accessLevel);
        if(group == null){
            throw  ErrorUtil.notFoundError("Group");
        }
        List<GroupListVO> groupVO = new ArrayList<>();
        groupListVOConverter.transferListOfModelToListOfVO(group, groupVO);
        return groupVO;
    }

    public GroupUpdateReponseVO getGroupForUpdate(String groupId) {
        if(!privilegeService.checkPrivilege(Constants.PrivilegeType.CREATE_GROUP)){
            throw ErrorUtil.notEligible("Melihat group");
        }
        if(groupId.equalsIgnoreCase("") || groupId.trim() == "") throw new NostraException("Group id wajib terisi");
        Group group = groupRepository.findBySecureId(groupId);
        if(group == null){
            throw  ErrorUtil.notFoundError("Group");
        }
        GroupUpdateReponseVO groupVO = new GroupUpdateReponseVO();
        List<PrivilegeListVO> privilegeListVOResponse = new ArrayList<>();
        groupVO.setName(group.getName());
        groupVO.setAccessLevel(group.getAccessLevel().getKeyName());
        groupVO.setId(group.getSecureId());

        //user
        List<Privilege> privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_USER);
        PrivilegeListVO privilegeListVOUser = checkPrivilegeInGroup(privileges,group.getPrivileges());
        privilegeListVOUser.setCategory(Constants.PrivilegeCategory.PRIVILEGE_USER);
        privilegeListVOResponse.add(privilegeListVOUser);

        //group
        privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_GROUP);
        PrivilegeListVO privilegeListVOGroup = checkPrivilegeInGroup(privileges,group.getPrivileges());
        privilegeListVOGroup.setCategory(Constants.PrivilegeCategory.PRIVILEGE_GROUP);
        privilegeListVOResponse.add(privilegeListVOGroup);

        //article
        privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_ARTICLE);
        PrivilegeListVO privilegeListVOArticle = checkPrivilegeInGroup(privileges,group.getPrivileges());
        privilegeListVOArticle.setCategory(Constants.PrivilegeCategory.PRIVILEGE_ARTICLE);
        privilegeListVOResponse.add(privilegeListVOArticle);

        //file
        privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_FILE);
        PrivilegeListVO privilegeListVOFile = checkPrivilegeInGroup(privileges,group.getPrivileges());
        privilegeListVOFile.setCategory(Constants.PrivilegeCategory.PRIVILEGE_FILE);
        privilegeListVOResponse.add(privilegeListVOFile);

        //smk
        privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_SMK);
        PrivilegeListVO privilegeListVOSmk = checkPrivilegeInGroup(privileges,group.getPrivileges());
        privilegeListVOSmk.setCategory(Constants.PrivilegeCategory.PRIVILEGE_SMK);
        privilegeListVOResponse.add(privilegeListVOSmk);

        //teacher
        privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_TEACHER);
        PrivilegeListVO privilegeListVOTeacher = checkPrivilegeInGroup(privileges,group.getPrivileges());
        privilegeListVOTeacher.setCategory(Constants.PrivilegeCategory.PRIVILEGE_TEACHER);
        privilegeListVOResponse.add(privilegeListVOTeacher);

        //student
        privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_STUDENT);
        PrivilegeListVO privilegeListVOStudent = checkPrivilegeInGroup(privileges,group.getPrivileges());
        privilegeListVOStudent.setCategory(Constants.PrivilegeCategory.PRIVILEGE_STUDENT);
        privilegeListVOResponse.add(privilegeListVOStudent);

        //report
        privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_REPORT);
        PrivilegeListVO privilegeListVOReport = checkPrivilegeInGroup(privileges,group.getPrivileges());
        privilegeListVOReport.setCategory(Constants.PrivilegeCategory.PRIVILEGE_REPORT);
        privilegeListVOResponse.add(privilegeListVOReport);

        groupVO.setPrivilegeList(privilegeListVOResponse);

        return groupVO;
    }


    @Transactional
    public Boolean updatePrivilege(AssignPrivilegeVO assignPrivilegeVO) {
        if (!privilegeService.checkPrivilege(Constants.PrivilegeType.UPDATE_GROUP)) {
            throw ErrorUtil.notEligible("Update Privilege in Group");
        }
        Group group = groupRepository.findBySecureId(assignPrivilegeVO.getGroupId());
        if (null == group) {
            throw ErrorUtil.notFoundError("Group");
        }
        if(assignPrivilegeVO.getNewName() != "" && assignPrivilegeVO.getNewName() != null && assignPrivilegeVO.getNewName().trim() != ""){
            group.setName(assignPrivilegeVO.getNewName());
        }
        List<String> listSecureId = new ArrayList<>();
        assignPrivilegeVO.getPrivilegeList().forEach(item -> {
            listSecureId.add(item.getId());
        });
        List<Privilege> privilegeForAdd = privilegeRepository.findBySecureIdIn(listSecureId);
        group.setPrivileges(privilegeForAdd);
        groupRepository.save(group);
        return Boolean.TRUE;
    }


    public Boolean deleteGroup(DeleteGroupRequestVO deleteGroupRequestVO){
        if (!privilegeService.checkPrivilege(Constants.PrivilegeType.DELETE_GROUP)) {
            throw ErrorUtil.notEligible("Delete group");
        }
        if(deleteGroupRequestVO.getGroupId().trim() == "" || deleteGroupRequestVO.getGroupId() == "") throw new NostraException("Group id wajib terisi");
        Group groupToDelete = groupRepository.findBySecureId(deleteGroupRequestVO.getGroupId());
        if(groupToDelete == null) throw ErrorUtil.notFoundError("Group");
        List<User> usersInGroup = userRepository.findByGroup(groupToDelete);
        if(usersInGroup.size() > 0) throw new NostraException("Terdapat user yang menggunakan group tersebut");
        groupRepository.deleteRow(groupToDelete);
        return true;
    }

    public Map<String, Object> listGroup(Integer page, Integer limit, String sortBy, String direction, String name) {
        sortBy = StringUtils.isEmpty(sortBy) ? "creationDate" : sortBy;
        direction = StringUtils.isEmpty(direction) ? "desc" : direction;
        name = StringUtils.isEmpty(name) ? "%" : "%"+name+"%";
        Pageable pageable = new PageRequest(page, limit, getSortBy(sortBy, direction));
        Page<Group> resultPage;
        resultPage = groupRepository.findByNameLike(name,pageable);
        List<Group> models = resultPage.getContent();
        Collection<GroupListVO> vos = groupListVOConverter.transferListOfModelToListOfVO(models, new ArrayList<>());
        return constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }


    private PrivilegeListVO checkPrivilegeInGroup(List<Privilege> privilegeListMaster,Collection<Privilege> privilegeListModel){
        Boolean check = true;
        List<PrivilegeVO> privilegeVOResult= new ArrayList<>();
        for(int i=0; i< privilegeListMaster.size();i++){
            PrivilegeVO privilegeVO = new PrivilegeVO();
            if(privilegeListModel.contains(privilegeListMaster.get(i))){
                privilegeVOConverter.transferModelToVO(privilegeListMaster.get(i),privilegeVO,true);
            }else{
                privilegeVOConverter.transferModelToVO(privilegeListMaster.get(i),privilegeVO,false);
                check = false;
            }
            privilegeVOResult.add(privilegeVO);
        }
        PrivilegeListVO privilegeListVO = new PrivilegeListVO();
        privilegeListVO.setValue(check);
        privilegeListVO.setPrivilegeVOList(privilegeVOResult);
        return privilegeListVO;
    }


    @Override
    protected BaseRepository<Group> getJpaRepository() {
        return groupRepository;
    }

    @Override
    protected JpaSpecificationExecutor<Group> getSpecRepository() {
        return groupRepository;
    }

    @Override
    protected IBaseVoConverter<GroupVO, GroupVO, Group> getVoConverter() {
        return groupVOConverter;
    }
}
