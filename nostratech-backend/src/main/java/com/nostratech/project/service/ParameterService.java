package com.nostratech.project.service;

import com.nostratech.project.converter.IBaseVoConverter;
import com.nostratech.project.converter.ParameterVoConverter;
import com.nostratech.project.persistence.domain.Parameter;
import com.nostratech.project.persistence.repository.BaseRepository;
import com.nostratech.project.persistence.repository.ParameterRepository;
import com.nostratech.project.vo.ParameterVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PanjiAlam on 8/29/2016.
 */
@Service
public class ParameterService extends BaseVoService<Parameter, ParameterVO, ParameterVO> {

    public static final Logger logger = LoggerFactory.getLogger(ParameterService.class);

    @Autowired
    ParameterRepository parameterRepository;

    @Autowired
    ParameterVoConverter parameterVoConverter;


    @Override
    protected BaseRepository<Parameter> getJpaRepository() {
        return parameterRepository;
    }

    @Override
    protected JpaSpecificationExecutor<Parameter> getSpecRepository() {
        return parameterRepository;
    }

    @Override
    protected IBaseVoConverter<ParameterVO, ParameterVO, Parameter> getVoConverter() {
        return parameterVoConverter;
    }

    public List<ParameterVO> getParameter(){

        String code="";

        List<Parameter> listOfParam = parameterRepository.findAll();
        List<ParameterVO> parameterVOs = new ArrayList<>();
        parameterVoConverter.transferListOfModelToListOfVO(listOfParam,parameterVOs);
        return parameterVOs;
    }

}
