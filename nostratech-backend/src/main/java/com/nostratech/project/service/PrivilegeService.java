package com.nostratech.project.service;

import com.nostratech.project.converter.GroupVOConverter;
import com.nostratech.project.converter.PrivilegeVOConverter;
import com.nostratech.project.vo.GroupSignInVO;
import com.nostratech.project.vo.PrivilegeListVO;
import com.nostratech.project.exception.NostraException;
import com.nostratech.project.persistence.domain.Group;
import com.nostratech.project.persistence.domain.User;
import com.nostratech.project.persistence.repository.PrivilegeRepository;
import com.nostratech.project.util.Constants;
import com.nostratech.project.util.ErrorUtil;
import com.nostratech.project.vo.BaseVO;
import com.nostratech.project.vo.PrivilegeVO;
import com.nostratech.project.vo.SignInResponseVO;
import com.nostratech.project.persistence.domain.Privilege;
import com.nostratech.project.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by rennytanuwijaya on 4/11/16.
 */

@Service
public class PrivilegeService {

    @Autowired
    SignInService signInService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PrivilegeRepository privilegeRepository;

    @Autowired
    PrivilegeVOConverter privilegeVOConverter;

    @Autowired
    GroupVOConverter groupVOConverter;

    public Boolean checkPrivilege(String privilegeName) {
        User user = signInService.getUser();
        if(null != user.getGroup()){
            Group result = user.getGroup();
            Boolean a = result.getPrivileges().stream().anyMatch(x -> x.getName().equals(privilegeName));
            if(a){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }

    @Transactional
    public List<PrivilegeListVO> getListOfPrivilege() {

        List<PrivilegeListVO> privilegeListVOResponse = new ArrayList<>();

        //user
        List<Privilege> privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_USER);
        List<PrivilegeVO> privilegeVOList = new ArrayList<>();
        privilegeVOConverter.transferListOfModelToListOfVO(privileges, privilegeVOList,false);
        PrivilegeListVO privilegeListVOUser = new PrivilegeListVO();
        privilegeListVOUser.setCategory(Constants.PrivilegeCategory.PRIVILEGE_USER);
        privilegeListVOUser.setPrivilegeVOList(privilegeVOList);
        privilegeListVOUser.setValue(false);
        privilegeListVOResponse.add(privilegeListVOUser);

        //group
        privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_GROUP);
        privilegeVOList = new ArrayList<>();
        privilegeVOConverter.transferListOfModelToListOfVO(privileges, privilegeVOList,false);
        PrivilegeListVO privilegeListVOGroup = new PrivilegeListVO();
        privilegeListVOGroup.setCategory(Constants.PrivilegeCategory.PRIVILEGE_GROUP);
        privilegeListVOGroup.setPrivilegeVOList(privilegeVOList);
        privilegeListVOGroup.setValue(false);
        privilegeListVOResponse.add(privilegeListVOGroup);

        //article
        privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_ARTICLE);
        privilegeVOList = new ArrayList<>();
        privilegeVOConverter.transferListOfModelToListOfVO(privileges, privilegeVOList,false);
        PrivilegeListVO privilegeListVOArticle = new PrivilegeListVO();
        privilegeListVOArticle.setCategory(Constants.PrivilegeCategory.PRIVILEGE_ARTICLE);
        privilegeListVOArticle.setPrivilegeVOList(privilegeVOList);
        privilegeListVOArticle.setValue(false);
        privilegeListVOResponse.add(privilegeListVOArticle);

        //file
        privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_FILE);
        privilegeVOList = new ArrayList<>();
        privilegeVOConverter.transferListOfModelToListOfVO(privileges, privilegeVOList,false);
        PrivilegeListVO privilegeListVOFile = new PrivilegeListVO();
        privilegeListVOFile.setCategory(Constants.PrivilegeCategory.PRIVILEGE_FILE);
        privilegeListVOFile.setPrivilegeVOList(privilegeVOList);
        privilegeListVOFile.setValue(false);
        privilegeListVOResponse.add(privilegeListVOFile);

        //smk
        privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_SMK);
        privilegeVOList = new ArrayList<>();
        privilegeVOConverter.transferListOfModelToListOfVO(privileges, privilegeVOList,false);
        PrivilegeListVO privilegeListVOSmk = new PrivilegeListVO();
        privilegeListVOSmk.setCategory(Constants.PrivilegeCategory.PRIVILEGE_SMK);
        privilegeListVOSmk.setPrivilegeVOList(privilegeVOList);
        privilegeListVOSmk.setValue(false);
        privilegeListVOResponse.add(privilegeListVOSmk);

        //teacher
        privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_TEACHER);
        privilegeVOList = new ArrayList<>();
        privilegeVOConverter.transferListOfModelToListOfVO(privileges, privilegeVOList,false);
        PrivilegeListVO privilegeListVOTeacher = new PrivilegeListVO();
        privilegeListVOTeacher.setCategory(Constants.PrivilegeCategory.PRIVILEGE_TEACHER);
        privilegeListVOTeacher.setPrivilegeVOList(privilegeVOList);
        privilegeListVOTeacher.setValue(false);
        privilegeListVOResponse.add(privilegeListVOTeacher);

        //student
        privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_STUDENT);
        privilegeVOList = new ArrayList<>();
        privilegeVOConverter.transferListOfModelToListOfVO(privileges, privilegeVOList,false);
        PrivilegeListVO privilegeListVOStudent = new PrivilegeListVO();
        privilegeListVOStudent.setCategory(Constants.PrivilegeCategory.PRIVILEGE_STUDENT);
        privilegeListVOStudent.setPrivilegeVOList(privilegeVOList);
        privilegeListVOStudent.setValue(false);
        privilegeListVOResponse.add(privilegeListVOStudent);

        //report
        privileges = privilegeRepository.findByCategory(Constants.PrivilegeCategory.PRIVILEGE_REPORT);
        privilegeVOList = new ArrayList<>();
        privilegeVOConverter.transferListOfModelToListOfVO(privileges, privilegeVOList,false);
        PrivilegeListVO privilegeListVOReport = new PrivilegeListVO();
        privilegeListVOReport.setCategory(Constants.PrivilegeCategory.PRIVILEGE_REPORT);
        privilegeListVOReport.setPrivilegeVOList(privilegeVOList);
        privilegeListVOReport.setValue(false);
        privilegeListVOResponse.add(privilegeListVOReport);

        return privilegeListVOResponse;
    }


    public SignInResponseVO userPrivilege() {
        User user = signInService.getUser();
        if(user == null){
            throw ErrorUtil.notFoundError("user");
        }
        GroupSignInVO groupVO = new GroupSignInVO();
        if(null != user.getGroup()){
            Group group = user.getGroup();
            if(group != null){
                groupVOConverter.transferModelToVOOnly(group, groupVO);
            }
        }
        if(user.getImmediateChangePassword() == false){
            Date lastUpdatedDate= user.getLastUpdatedPassword();
            Date today = new Date();
            long diff = today.getTime() - lastUpdatedDate.getTime();
            long diffDays = diff / (24 * 60 * 60 * 1000);
            if(diffDays > 90){
                user.setImmediateChangePassword(true);
                userRepository.save(user);
            }
        }

        SignInResponseVO signInResponseVO = new SignInResponseVO();
        signInResponseVO.setAccessToken(user.getAccessToken());
        signInResponseVO.setImmediateChangePassword(user.getImmediateChangePassword());
        signInResponseVO.setGroup(groupVO);
        return  signInResponseVO;
    }

}
