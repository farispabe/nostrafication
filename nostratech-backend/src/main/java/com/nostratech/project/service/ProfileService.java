package com.nostratech.project.service;

import com.nostratech.project.persistence.domain.*;
import com.nostratech.project.persistence.repository.*;
import com.nostratech.project.vo.*;
import com.nostratech.project.exception.NostraException;
import com.nostratech.project.util.Constants;
import com.nostratech.project.util.ErrorUtil;
import com.nostratech.project.util.ExtendedSpringBeanUtil;
import com.nostratech.project.validator.ProfileValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

/**
 * agus w
 */

@Service
public class ProfileService{

    public static final Logger logger = LoggerFactory.getLogger(ProfileService.class);

    @Autowired
    SignInService signInService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AccessTokenRepository accessTokenRepository;

    @Autowired
    @Qualifier("passwordEncoder")
    PasswordEncoder passwordEncoder;

    @Autowired
    ProfileValidator profileValidator;

    @Value("${client.id.user}")
    private String clientIdUser;

    @Value("${client.id.admin}")
    private String clientIdAdmin;

    @Autowired
    PrivilegeService privilegeService;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    ProfileAdminRepository profileAdminRepository;

    @Autowired
    ProfileUserRepository profileUserRepository;


    @Transactional
    public Boolean editUser(ChangeProfileUserRequestVO profileRequestVO) {
        if(!privilegeService.checkPrivilege("update_user")){
            throw ErrorUtil.notEligible("Update User");
        }
        String messageValidation = profileValidator.validateChangeProfileUser(profileRequestVO);

        if (null != messageValidation) throw new NostraException(messageValidation);

        User user = userRepository.findBySecureId(profileRequestVO.getUserId());

        if (null == user) throw new NostraException("Your profile not found");

        ExtendedSpringBeanUtil.copySpecificProperties(profileRequestVO, user,
                new String[]{"email", "active"});

        userRepository.save(user);

        String accessTokenString = userRepository.findAccessTokenByEmail(user.getEmail());
        AccessToken accessTokenData = accessTokenRepository.findByAccessToken(accessTokenString);
        if(accessTokenData.getClientId().equalsIgnoreCase("hq")){
            ProfileAdmin profileAdmin = profileAdminRepository.findByUserId(user.getId());
            if(null == profileAdmin) throw new NostraException("Your profile not found");
            ExtendedSpringBeanUtil.copySpecificProperties(profileRequestVO, profileAdmin,
                    new String[]{"fullName"});
            profileAdminRepository.saveAndFlush(profileAdmin);

        }else if(accessTokenData.getClientId().equalsIgnoreCase("md")){
            ProfileUser profileUser = profileUserRepository.findByUserId(user.getId());
            if(null == profileUser) throw new NostraException("Your profile not found");
            ExtendedSpringBeanUtil.copySpecificProperties(profileRequestVO, profileUser,
                    new String[]{"fullName"});
            profileUserRepository.saveAndFlush(profileUser);
        }

        return Boolean.TRUE;
    }

    public Object changePasswordUser(ChangePasswordRequestVO changePasswordRequestVO) {

        String username = signInService.getUsername();

        User user = userRepository.findByUsername(username);

        if (null == user) throw new NostraException("Your profile not found");

        String messageValidation = profileValidator.validateChangePassword(changePasswordRequestVO, user.getPassword());

        if (null != messageValidation) throw new NostraException(messageValidation);

        user.setPassword(passwordEncoder.encode(changePasswordRequestVO.getNewPassword()));
        user.setImmediateChangePassword(false);
        user.setLastUpdatedPassword(new Date());
        userRepository.saveAndFlush(user);

        return passwordEncoder.matches(changePasswordRequestVO.getNewPassword(), user.getPassword());
    }

    public ProfileUserVO getUserProfile() {
        String username = signInService.getUsername();

        User admin = userRepository.findByUsername(username);

        if (null == admin) throw new NostraException("Your profile not found");

        ProfileVO profileVO = new ProfileVO();

        ExtendedSpringBeanUtil.copySpecificProperties(admin, profileVO,
                new String[]{"email", "fullName", "address"});

        return profileVO;
    }


    public Boolean assignGroup(AssignGroupVO assignGroupVO) {
        if(!privilegeService.checkPrivilege("create_user")){
            throw ErrorUtil.notEligible("Assign Group To User");
        }

        String messageValidation = profileValidator.validateAssignGroup(assignGroupVO);

        if (null != messageValidation) throw ErrorUtil.notFoundError("Group");

        User user = userRepository.findBySecureId(assignGroupVO.getUserId());
        if(null == user){
            throw  ErrorUtil.notFoundError("User");
        }

        Group group = groupRepository.findBySecureId(assignGroupVO.getGroupId());
        if(null == group){
            throw  ErrorUtil.notFoundError("Group");
        }

        if(null != user.getGroup() && !assignGroupVO.getForce()){
            throw new NostraException("This User Already in Group");
        }
        user.setGroup(group);
        userRepository.saveAndFlush(user);

        return Boolean.TRUE;
    }
}
