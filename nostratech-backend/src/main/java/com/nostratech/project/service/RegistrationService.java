package com.nostratech.project.service;

import com.nostratech.project.persistence.domain.Group;
import com.nostratech.project.persistence.repository.GroupRepository;
import com.nostratech.project.exception.NostraException;
import com.nostratech.project.oauth.token.store.NostraTokenStore;
import com.nostratech.project.persistence.domain.*;
import com.nostratech.project.persistence.repository.*;
import com.nostratech.project.util.*;
import com.nostratech.project.validator.RegistrationValidator;
import com.nostratech.project.vo.BaseVO;
import com.nostratech.project.vo.RegistrationUserRequestVO;
import com.nostratech.project.vo.ResetPasswordVO;
import com.nostratech.project.vo.mail.EmailVO;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * agus w
 */

@Service
@Transactional(readOnly = true)
public class RegistrationService {

    public static final Logger logger = LoggerFactory.getLogger(RegistrationService.class);

    @Autowired
    ProfileAdminRepository profileAdminRepository;

    @Autowired
    ProfileUserRepository profileUserRepository;

    @Autowired
    RegistrationValidator registrationValidator;

    @Autowired
    UserRepository userRepository;

    @Autowired
    @Qualifier("passwordEncoder")
    PasswordEncoder passwordEncoder;

    @Autowired
    NostraTokenStore nostraTokenStore;

    @Autowired
    EmailService emailService;

    @Autowired
    SignInService signInService;

    @Value(value = "${default.password}")
    String defaultPassword;

    @Value(value = "${reset.password}")
    String resetPasswordTemplate;

    @Value(value = "${reset.password.title}")
    String resetPasswordTitle;

    @Value(value = "${create.user.template}")
    String createUserTemplate;

    @Value(value = "${create.user.title}")
    String createUserTitle;

    @Value("${client.id.admin}")
    String clientIdAdmin;

    @Value("${client.id.user}")
    String clientIdUser;

    @Value("${default.email.sender}")
    String defaultEmailSender;

    @Autowired
    PrivilegeService privilegeService;

    @Autowired
    GroupRepository groupRepository;

    @Transactional
    public BaseVO registrationUser(RegistrationUserRequestVO userRegistration) {
        if(!privilegeService.checkPrivilege("create_user")){
            throw ErrorUtil.notEligible("Create User");
        }
        String messageValidation = registrationValidator.validateRegistrationUser(userRegistration, clientIdAdmin);
        if (null != messageValidation) throw new NostraException(messageValidation);
        User userFound = userRepository.findByUsernameOrEmail(userRegistration.getUsername(),userRegistration.getEmail());
        if(userFound != null){
            if(userFound.getEmail().equalsIgnoreCase(userRegistration.getEmail()))
                throw new NostraException("Email sudah terdaftar");
            if(userFound.getUsername().equalsIgnoreCase(userRegistration.getUsername()))
                throw new NostraException("Username sudah terdaftar");
        }
        User user = new User();
        Group group = groupRepository.findBySecureId(userRegistration.getGroupId());
        if (group == null){
            throw ErrorUtil.notFoundError("Group");
        }
        user.setGroup(group);
        ExtendedSpringBeanUtil.copySpecificProperties(userRegistration, user,
                new String[]{"email","username"},
                new String[]{"email","username"});

        String encodePassword = passwordEncoder.encode(userRegistration.getPassword());

        user.setPassword(encodePassword);
        user.setClientId(userRegistration.getRole());
        User added = userRepository.save(user);
        if (null == added) throw new NostraException("Registration user is failed");

        if(userRegistration.getRole().equals(clientIdAdmin)){
            ProfileAdmin profileAdmin = new ProfileAdmin();
            ExtendedSpringBeanUtil.copySpecificProperties(userRegistration, profileAdmin,
                    new String[]{"fullName"},
                    new String[]{"fullName"});
            profileAdmin.setUser(added);
            profileAdminRepository.save(profileAdmin);
            nostraTokenStore.storeAccessToken(added.getAccessToken(), clientIdAdmin);
        }else if(userRegistration.getRole().equals(clientIdUser)){
            ProfileUser profileUser = new ProfileUser();
            ExtendedSpringBeanUtil.copySpecificProperties(userRegistration, profileUser,
                    new String[]{"fullName"},
                    new String[]{"fullName"});
            profileUser.setUser(added);
            profileUserRepository.save(profileUser);
            nostraTokenStore.storeAccessToken(added.getAccessToken(), clientIdUser);
        }else{
            throw ErrorUtil.notFoundError("ROLE");
        }

        BaseVO response = new BaseVO();
        response.setId(added.getSecureId());
        return response;
    }


    public Boolean checkUserEmail(String email) {
        return null != userRepository.findByEmail(email);
    }

    public Boolean resetPasswordUser(ResetPasswordVO resetPassword) {
        if (StringUtils.isEmpty(resetPassword.getEmail()))
            throw new NostraException(StatusCode.ERROR, "Email is empty");

        String newPassword = PasswordUtil.generatePassword();

        String encodedPassword = passwordEncoder.encode(newPassword);

        User user = userRepository.findByEmail(resetPassword.getEmail());

        if (null == user){
            logger.error("Reset password failed, User not found");
            return true;
        }else if(null != user && user.getActive() == false){
            logger.error("Reset password failed, User not active. Username: "+ user.getUsername());
            return true;
        }

        user.setPassword(encodedPassword);
        user.setImmediateChangePassword(true);
        userRepository.saveAndFlush(user);

        return resetPassword("there", newPassword, resetPassword.getEmail());
    }

    private boolean resetPassword(String name, String newPassword, String email) {

        final Map<String, Object> map = new HashMap<String, Object>();
        map.put("userName", name);
        map.put("password", newPassword);
        EmailVO emailMessageVO = new EmailVO();
        emailMessageVO.setDestination(email);
        emailMessageVO.setSender(defaultEmailSender);
        emailMessageVO.setSubject(resetPasswordTitle);
        emailMessageVO.setTemplateLocation(resetPasswordTemplate);
        emailMessageVO.setTemplateValues(map);
        return emailService.send(emailMessageVO);

    }

    private boolean createUserEmailNotification(String creatorName, String userNameLogin, String passwordLogin,
                                                String emailLogin, String domain, String domainName,  String email) {

        final Map<String, Object> map = new HashMap<String, Object>();
        map.put("creatorName", creatorName);
        map.put("userNameLogin", userNameLogin);
        map.put("passwordLogin", passwordLogin);
        map.put("emailLogin", emailLogin);
        map.put("domain", domain);
        map.put("domainName", domainName);

        EmailVO emailMessageVO = new EmailVO();
        emailMessageVO.setDestination(email);
        emailMessageVO.setSender(defaultEmailSender);
        emailMessageVO.setSubject(createUserTitle);
        emailMessageVO.setTemplateLocation(createUserTemplate);
        emailMessageVO.setTemplateValues(map);
        return emailService.send(emailMessageVO);

    }
}
