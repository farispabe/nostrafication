package com.nostratech.project.service;

import com.nostratech.project.converter.GroupVOConverter;
import com.nostratech.project.persistence.domain.*;
import com.nostratech.project.persistence.repository.*;
import com.nostratech.project.vo.GroupSignInVO;
import com.nostratech.project.oauth.token.store.NostraTokenStore;
import com.nostratech.project.exception.NostraException;
import com.nostratech.project.util.ApplicationContextProvider;
import com.nostratech.project.vo.SignInRequestVO;
import com.nostratech.project.vo.SignInResponseVO;
import com.nostratech.project.vo.SignOutRequestVO;
import com.nostratech.project.util.Constants;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * agus w
 */

@Service
@Transactional(readOnly = true)
public class SignInService {

    public static final Logger logger = LoggerFactory.getLogger(SignInService.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    @Qualifier("passwordEncoder")
    PasswordEncoder passwordEncoder;

    @Autowired
    AccessTokenRepository accessTokenRepository;

    @Value("${client.id.admin}")
    String clientIdAdmin;

    @Value("${client.id.user}")
    String clientIdUser;

    @Value("${client.id.hq}")
    String clientIdHq;

    @Value("${client.id.md}")
    String clientIdmd;

    @Value("${client.id.smk}")
    String clientIdsmk;

    @Autowired
    ProfileAdminRepository profileAdminRepository;

    @Autowired
    ProfileUserRepository profileUserRepository;

    @Autowired
    GroupVOConverter groupVOConverter;

    @Autowired
    ParameterRepository parameterRepository;

    private SignInResponseVO doLoginUser(String reqPassword, //String password, String accessToken,
                                     User user,//Boolean changePassword,
                                         GroupSignInVO groupVO){
        //user.getPassword(), user.getAccessToken(), user.getImmediateChangePassword()
        if (passwordEncoder.matches(reqPassword, user.getPassword())) {

            NostraTokenStore nostraTokenStore = ApplicationContextProvider.getApplicationContext().getBean(NostraTokenStore.class);

            nostraTokenStore.storeAccessToken(user.getAccessToken(), user.getClientId());

            AccessToken token = accessTokenRepository.findByAccessToken(user.getAccessToken());

            String fullName = "";
            String domain = "";

            if(token.getClientId().equals(clientIdAdmin)){
                ProfileAdmin profileUserHq = profileAdminRepository.findByUserId(user.getId());
                fullName = profileUserHq.getFullName();
                domain = "ADMIN";
            } else if(token.getClientId().equals(clientIdUser)){
                ProfileUser profileUserMd = profileUserRepository.findByUserId(user.getId());
                fullName = profileUserMd.getFullName();
                domain = "USER";
            }

            SignInResponseVO signInResponseVO = new SignInResponseVO();
            signInResponseVO.setAccessToken(user.getAccessToken());
            signInResponseVO.setImmediateChangePassword(user.getImmediateChangePassword());
            signInResponseVO.setFullName(fullName);
            signInResponseVO.setGroup(groupVO);
            signInResponseVO.setDomain(domain);

            return signInResponseVO;
        }else{
            throw new NostraException("Username atau password tidak valid");
        }
    }

    public SignInResponseVO loginUser(SignInRequestVO signInRequest){

        User user = userRepository.findByUsername(signInRequest.getUsername());

        if(null == user) {
            throw new NostraException("Username atau password tidak valid");
        }

        if(!user.getActive()){
            throw new NostraException("Akun tidak aktif");
        }

        Parameter expired = parameterRepository.findByCode(Constants.ParamCode.PASSWORD);
        int passwordChange = Integer.parseInt(expired.getValue());
        if(user.getImmediateChangePassword() == false){
            Date lastUpdatedDate= user.getLastUpdatedPassword();
            Date today = new Date();
            long diff = today.getTime() - lastUpdatedDate.getTime();
            long diffDays = diff / (24 * 60 * 60 * 1000);
            if(diffDays > passwordChange){
                user.setImmediateChangePassword(true);
                userRepository.save(user);
            }
        }

        Group group = user.getGroup();
        GroupSignInVO groupVO = new GroupSignInVO();
        groupVOConverter.transferModelToVOOnly(group, groupVO);
        return doLoginUser(signInRequest.getPassword(), user, groupVO);
    }

    public String getUsername(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null) {
            throw new NostraException("User is not authenticated");
        }

        return authentication.getName();
    }

    public User getUser(){
        return userRepository.findByUsername(getUsername());
    }

    public Boolean logout(String accessTokenStr) {

        if (StringUtils.isEmpty(accessTokenStr)) throw new NostraException("access token wajib terisi");

        AccessToken accessToken = accessTokenRepository.findByAccessToken(accessTokenStr);

        if (null == accessToken) throw new NostraException("access token tidak ditemukan");

        accessTokenRepository.delete(accessToken);

        return Boolean.TRUE;
    }

    public Boolean logout(SignOutRequestVO signOutRequestVO) {

        if (StringUtils.isEmpty(signOutRequestVO.getAccessToken())) throw new NostraException("access_token is empty");

        AccessToken accessToken = accessTokenRepository.findByAccessToken(signOutRequestVO.getAccessToken());

        if (null == accessToken) throw new NostraException("access_token not found");

        accessTokenRepository.delete(accessToken);

        return Boolean.TRUE;
    }

    public Boolean isValid(String accessTokenStr) {

        if (!StringUtils.isEmpty(accessTokenStr)) {
            AccessToken accessToken = accessTokenRepository.findByAccessToken(accessTokenStr);

            if (null != accessToken && !accessToken.getDeleted()){
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

}
