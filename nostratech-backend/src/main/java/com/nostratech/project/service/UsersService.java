package com.nostratech.project.service;

import com.nostratech.project.converter.UserVoConverter;
import com.nostratech.project.persistence.domain.*;
import com.nostratech.project.persistence.repository.GroupRepository;
import com.nostratech.project.persistence.domain.ProfileAdmin;
import com.nostratech.project.persistence.repository.ProfileAdminRepository;
import com.nostratech.project.persistence.repository.ProfileUserRepository;
import com.nostratech.project.persistence.repository.UserRepository;
import com.nostratech.project.converter.IBaseVoConverter;
import com.nostratech.project.exception.NostraException;
import com.nostratech.project.persistence.repository.*;
import com.nostratech.project.util.Constants;
import com.nostratech.project.util.ErrorUtil;
import com.nostratech.project.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Renny on 4/29/16.
 */
@Service
public class UsersService extends BaseVoService<User, UserVO, UserVO> {


    @Autowired
    ProfileAdminRepository profileAdminRepository;

    @Autowired
    ProfileUserRepository profileUserRepository;

    @Autowired
    SignInService signInService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PrivilegeService privilegeService;

    @Autowired
    AccessTokenRepository accessTokenRepository;

    @Autowired
    UserVoConverter userVoConverter;

    @Autowired
    GroupRepository groupRepository;

    @Value("${client.id.admin}")
    String clientIdAdmin;

    @Value("${client.id.user}")
    String clientIdUser;

    @Transactional
    public Boolean deleteUser(DeleteUserRequestVO deleteUserRequestVO){

        User user = signInService.getUser();

        User userToDelete = userRepository.findBySecureId(deleteUserRequestVO.getId());
        AccessToken token = accessTokenRepository.findByAccessToken(userToDelete.getAccessToken());

        if(!privilegeService.checkPrivilege(Constants.PrivilegeType.DELETE_USER)){
            throw ErrorUtil.notEligible("Delete User Failed");
        }
        if(user==null){
            throw ErrorUtil.notFoundError("User");
        }

        if(userToDelete==null){
            throw ErrorUtil.notFoundError("User is not identified");
        }

        if(token.getClientId().equals(clientIdAdmin)){
            ProfileAdmin profileAdmin = profileAdminRepository.findByUserId(userToDelete.getId());
            profileAdminRepository.deleteRow(profileAdmin);

        }
        else if(token.getClientId().equals(clientIdUser)){
            ProfileUser profileUser = profileUserRepository.findByUserId(userToDelete.getId());
            profileUserRepository.deleteRow(profileUser);
        }
        accessTokenRepository.deleteRow(token);

        User deleteUser = userRepository.findBySecureId(deleteUserRequestVO.getId());

        userRepository.deleteRow(deleteUser);
        return true;
    }

    public DetailUserVO getDetailsUser(String secureId){

        User user = userRepository.findBySecureId(secureId);

        if(user==null){
            throw ErrorUtil.notFoundError("User");
        }

        AccessToken token = accessTokenRepository.findByAccessToken(user.getAccessToken());

        DetailUserVO detailUserVO = new DetailUserVO();

        if(token.getClientId().equals(clientIdAdmin)){
            ProfileAdmin profileAdmin = profileAdminRepository.findByUserId(user.getId());
            detailUserVO.setFullName(profileAdmin.getFullName());
        }
        else if(token.getClientId().equals(clientIdUser)){
            ProfileUser profileUser = profileUserRepository.findByUserId(user.getId());
            detailUserVO.setFullName(profileUser.getFullName());
        }

        userVoConverter.transferModelToVONew(user,detailUserVO);
        return detailUserVO;
    }

    @Transactional
    public BaseVO updateUser(UpdateUserRequestVO updateUserRequestVO){
        if(!privilegeService.checkPrivilege(Constants.PrivilegeType.UPDATE_USER)){
            throw ErrorUtil.notEligible("Update User");
        }

        if(updateUserRequestVO.getId()==null || updateUserRequestVO.getId()==""){
            throw new NostraException("Id user wajib terisi");
        }

        if(updateUserRequestVO.getFullName()==null || updateUserRequestVO.getFullName()==""){
            throw new NostraException("Nama lengkap wajib terisi");
        }

        if(updateUserRequestVO.getEmail()==null || updateUserRequestVO.getEmail()==""){
            throw new NostraException("Email wajib terisi");
        }

        if(updateUserRequestVO.getPhoneNumber()==null || updateUserRequestVO.getPhoneNumber()==""){
            throw new NostraException("Nomor telephone wajib terisi");
        }
        if(updateUserRequestVO.getActive()==null){
            throw new NostraException("Status user wajib diisi");
        }

        User userContent = userRepository.findBySecureId(updateUserRequestVO.getId());

        if(userContent == null){
            throw ErrorUtil.notEligible("User");
        }

        User userEmail = userRepository.findByEmail(updateUserRequestVO.getEmail());
        if(userEmail != null && !userEmail.getSecureId().equals(updateUserRequestVO.getId())){
            throw new NostraException("Email sudah terdaftar");
        }

        AccessToken token = accessTokenRepository.findByAccessToken(userContent.getAccessToken());

        userContent.setEmail(updateUserRequestVO.getEmail());
        userContent.setActive(updateUserRequestVO.getActive());
        Group group = groupRepository.findBySecureId(updateUserRequestVO.getGroupId());
        userContent.setGroup(group);

        if(token.getClientId().equals(clientIdAdmin)){
            ProfileAdmin profileAdmin = profileAdminRepository.findByUserId(userContent.getId());
            profileAdmin.setFullName(updateUserRequestVO.getFullName());
            profileAdminRepository.save(profileAdmin);
        }
        else if(token.getClientId().equals(clientIdUser)){
            ProfileUser profileUser = profileUserRepository.findByUserId(userContent.getId());
            profileUser.setFullName(updateUserRequestVO.getFullName());
            profileUserRepository.save(profileUser);
        }

        BaseVO response = new BaseVO();
        User saved = userRepository.save(userContent);
        response.setId(saved.getSecureId());
        return response;
    }

    @Override
    protected BaseRepository<User> getJpaRepository() {
        return null;
    }

    @Override
    protected JpaSpecificationExecutor<User> getSpecRepository() {
        return null;
    }

    @Override
    protected IBaseVoConverter<UserVO, UserVO, User> getVoConverter() {
        return null;
    }
}
