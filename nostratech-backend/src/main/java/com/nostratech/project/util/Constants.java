package com.nostratech.project.util;

public class Constants {

    public static class Multiplier {
        public static final Integer MINUS = 0;
        public static final Integer PLUS = 1;
    }


    public static final class RequestStatus {
        public static final String PENDING = "PENDING";
        public static final String APPROVED = "APPROVED";
        public static final String REJECTED = "REJECTED";
    }

    public static final class PageParameter {
        public static final String LIST_DATA = "listData";
        public static final String TOTAL_PAGES = "totalPages";
        public static final String TOTAL_ELEMENTS = "totalElements";
    }

    public static final class GroupType {
        public static final String HEAD_QUARTER = "HQ";
        public static final String MAIN_DEALER = "MainDealer";
        public static final String SMK = "SMK";
    }


    public static final class PrivilegeType{
        public static final String CREATE_USER = "create_user";
        public static final String UPDATE_USER = "update_user";
        public static final String READ_USER = "read_user";
        public static final String DELETE_USER = "delete_user";

        public static final String CREATE_GROUP = "create_group";
        public static final String UPDATE_GROUP = "update_group";
        public static final String READ_GROUP = "read_group";
        public static final String DELETE_GROUP = "delete_group";

        public static final String CREATE_ARTICLE = "create_article";
        public static final String UPDATE_ARTICLE = "update_article";
        public static final String READ_ARTICLE = "read_article";
        public static final String DELETE_ARTICLE = "delete_article";

        public static final String CREATE_FILE = "create_file";
        public static final String UPLOAD_FILE = "upload_file";
        public static final String UPDATE_FILE = "update_file";
        public static final String READ_FILE = "read_file";
        public static final String DELETE_FILE = "delete_file";
        public static final String DOWNLOAD_FILE = "download_file";


        public static final String CREATE_SMK = "create_smk";
        public static final String UPDATE_SMK = "update_smk";
        public static final String UPDATE_STATIC_SMK = "update_static_smk";

        public static final String READ_SMK = "read_smk";
        public static final String DELETE_SMK = "delete_smk";
        public static final String VERIFY_DATA_SMK = "verify_data_smk";
        public static final String READ_VERIFY_DATA_SMK = "read_verify_data_smk";
        public static final String EXPORT_EXCEL_SMK = "export_excel_smk";

        public static final String CREATE_TEACHER = "create_teacher";
        public static final String UPDATE_TEACHER = "update_teacher";
        public static final String READ_TEACHER = "read_teacher";
        public static final String DELETE_TEACHER = "delete_teacher";
        public static final String VERIFY_DATA_TEACHER = "verify_data_teacher";
        public static final String READ_VERIFY_DATA_TEACHER = "read_verify_data_teacher";
        public static final String EXPORT_EXCEL_TEACHER = "export_excel_teacher";

        public static final String CREATE_STUDENT = "create_student";
        public static final String UPDATE_STUDENT = "update_student";
        public static final String DELETE_STUDENT = "delete_student";
        public static final String READ_STUDENT = "read_student";
        public static final String CHANGE_CLASS = "change_class";
        public static final String VERIFY_DATA_STUDENT= "verify_data_student";
        public static final String READ_VERIFY_DATA_STUDENT = "read_verify_data_student";
        public static final String EXPORT_EXCEL_STUDENT = "export_excel_student";
        public static final String UPLOAD_EXCEL_DATA_STUDENT = "upload_data_siswa";

        public static final String REPORT_ACTIVITY = "report_activity";
        public static final String REPORT_HIGHLIGHT = "report_highlight";
        public static final String REPORT_EXCEL = "report_excel";

    }

    public static final class PrivilegeCategory{
        public static final String PRIVILEGE_USER = "Pengguna";
        public static final String PRIVILEGE_GROUP = "Grup";
        public static final String PRIVILEGE_ARTICLE = "Konten";
        public static final String PRIVILEGE_FILE = "Document";
        public static final String PRIVILEGE_SMK = "SMK";
        public static final String PRIVILEGE_TEACHER = "Guru";
        public static final String PRIVILEGE_STUDENT = "Siswa";
        public static final String PRIVILEGE_REPORT = "Report";
    }

    public static final class CategoryVacancy{
        public static final String AHM = "AHM";
        public static final String AHAS = "AHAS";
        public static final String MAIN_DEALER = "MAIN_DEALER";
    }

    public static final class NotificationKey{
        public static final String PASSWORD = "PASSWORD";
        public static final String NEW_MATERIAL = "NEW_MATERIAL";
        public static final String MOU_SMK = "MOU_SMK";
        public static final String MOU_TUK = "MOU_TUK";
        public static final String UPDATE_DATA_SMK = "UPDATE_DATA_SMK";
        public static final String UPDATE_DATA_TEACHER = "UPDATE_DATA_TEACHER";
        public static final String UPDATE_DATA_STUDENT = "UPDATE_DATA_STUDENT";
    }

    public static final class ParamCode{
        public static final String PASSWORD = "PASSWORD";
        public static final String NEW_MATERIAL = "NEW_MATERIAL";
        public static final String MOU = "MOU";
        public static final String ADMIN_EMAIL = "ADMIN_EMAIL";
        public static final String PASSWORD_NOTIFICATION = "PASSWORD_NOTIFICATION";
        public static final String LOGIN_EXPIRED = "LOGIN_EXPIRED";
        public static final String LAST_MOU_EXPIRED = "LAST_MOU_EXPIRED";
        public static final String SCHEDULE_GRADUATE_STUDENT = "SCHEDULE_GRADUATE_STUDENT";
        public static final String SCHEDULE_SYNC_MAIN_DELAER = "SCHEDULE_SYNC_MAIN_DEALER";
        public static final String SCHEDULE_SYNC_PROVINCE = "SCHEDULE_SYNC_PROVINCE";
        public static final String SCHEDULE_SYNC_CITY = "SCHEDULE_SYNC_CITY";
    }


    public static final class StatusData {
        public static final String NOT_VERIFIED = "NOT_VERIFIED";
        public static final String VERIFIED = "VERIFIED";
    }

    public static final class ToolCategory {
        public static final String PGM_FI_TOOLS = "PGM_FI_TOOLS";
        public static final String CLUTCH_INSTALLER_REMOVER = "CLUTCH_INSTALLER_REMOVER";
        public static final String FLYWHEEL_REMOVER_INSTALLER = "FLYWHEEL_REMOVER_INSTALLER";
        public static final String DRIVEN_PULLEY_HOLDER = "DRIVEN_PULLEY_HOLDER";
        public static final String VALVE_REMOVER = "VALVE_REMOVER";
        public static final String STRATEGIC_TOOLS = "STRATEGIC_TOOLS";
        public static final String MEASUREMENT_TOOLS = "MEASUREMENT_TOOLS";
        public static final String COMMON_TOOLS = "COMMON_TOOLS";
        public static final String COMPLEMENTARY_TOOLS = "COMPLEMENTARY_TOOLS";
    }

    public static final class Schedule{
        public static final String PROVINSI = "PROVINSI";
        public static final String KOTA = "KOTA";
        public static final String KECAMATAN = "KECAMATAN";
        public static final String KELURAHAN = "KELURAHAN";
        public static final String MAIN_DEALER = "MAIN_DEALER";
        public static final String SOFT_LINK = "SOFT_LINK";
    }

    public static final class TypePicSmk{
        public static final String SCHOOL = "SCHOOL";
        public static final String LAB = "LAB";
        public static final String LAB_ELECTRICITY = "LAB_ELECTRICITY";
        public static final String LAB_TROUBLESHOOT = "LAB_TROUBLESHOOT";
        public static final String LAB_MACHINE = "LAB_MACHINE";
    }

    public static final class StudentUpdateType {
        public static final String UPDATE = "UPDATE";
        public static final String UPLOAD = "UPLOAD";
        public static final String CLASS_GRADUATE_1 = "CLASS_GRADUATE_1";
        public static final String CLASS_GRADUATE_2 = "CLASS_GRADUATE_2";
        public static final String CLASS_GRADUATE_3 = "CLASS_GRADUATE_3";
    }

    public static final class Religion {
        public static final String RELIGION_ISLAM = "Islam";
        public static final String RELIGION_KRISTEN = "Kristen";
        public static final String RELIGION_KATOLIK = "Katolik";
        public static final String RELIGION_BUDDHA = "Buddha";
        public static final String RELIGION_HINDU = "Hindu";
        public static final String RELIGION_LAIN_LAIN = "Lain-Lain";
    }

    public static final class ActivityType{
        public static final String DOWNLOAD_DOCUMENT = "DOWNLOAD DOCUMENT";
        public static final String UPDATE_DATA = "UPDATE DATA";
        public static final String VERIFY_DATA = "VERIFY_DATA";
    }
}
