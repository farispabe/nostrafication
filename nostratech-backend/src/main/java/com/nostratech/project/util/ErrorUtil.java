package com.nostratech.project.util;

import com.nostratech.project.exception.NostraException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by antin on 1/7/16.
 */
@Component
public class ErrorUtil {

    private static String notNull;

    private static String notFound;

    private static String isExist;

    private static String notEligible;

    @Value("${error.not.null}")
    public void setNotNull(String notNull) {
       ErrorUtil.notNull = notNull;
    }

    @Value("${error.not.found}")
    public void setNotFound(String notFound) {
        ErrorUtil.notFound = notFound;
    }

    @Value("${error.is.exist}")
    public void setIsExist(String isExist) {
        ErrorUtil.isExist = isExist;
    }

    @Value("${error.not.eligible}")
    public void setNotEligible(String notEligible) {
        ErrorUtil.notEligible = notEligible;
    }

    public static NostraException notNullError(String var) {
        return new NostraException(String.format(notNull, var));
    }

    public static NostraException notFoundError(String var) {
        return new NostraException(String.format(notFound, var));
    }

    /**
     *  You cant't add, the %s data with %s is %s already exist. You should be execute edit action.
     * @return
     */
    public static NostraException isExist(String identifier, String field, String value) {
        return new NostraException(String.format(isExist, identifier, field, value));
    }

    public static NostraException notEligible(String var) {
        return new NostraException(String.format(notEligible, var));
    }

    public static NostraException notAuthorize() {
        return new NostraException("You're not authorize.");
    }
}
