package com.nostratech.project.util;

import com.nostratech.project.persistence.domain.AccessToken;
import com.nostratech.project.persistence.domain.ProfileAdmin;
import com.nostratech.project.persistence.repository.AccessTokenRepository;
import com.nostratech.project.service.SignInService;
import com.nostratech.project.persistence.domain.ProfileUser;
import com.nostratech.project.persistence.domain.User;
import com.nostratech.project.persistence.repository.ProfileAdminRepository;
import com.nostratech.project.persistence.repository.ProfileUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by Renny on 9/11/16.
 */
@Component
public class HierarchyUtil {

    @Autowired
    SignInService signInService;

    @Autowired
    AccessTokenRepository accessTokenRepository;

    @Value("${client.id.admin}")
    String clientIdAdmin;

    @Value("${client.id.user}")
    String clientIdUser;


    @Autowired
    ProfileUserRepository profileUserRepository;

    @Autowired
    ProfileAdminRepository profileAdminRepository;

    public Boolean isUser(){
        User user = signInService.getUser();
        AccessToken accessToken = accessTokenRepository.findByAccessToken(user.getAccessToken());
        if(accessToken.getClientId().equals(clientIdAdmin)) {
            return true;
        }
        return false;
    }

    public Boolean isAdmin(){
        User user = signInService.getUser();
        AccessToken accessToken = accessTokenRepository.findByAccessToken(user.getAccessToken());
        if(accessToken.getClientId().equals(clientIdUser)) {
            return true;
        }
        return false;
    }

}
