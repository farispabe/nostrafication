package com.nostratech.project.util;

import com.nostratech.project.persistence.domain.AccessLevel;
import com.nostratech.project.persistence.domain.Privilege;
import com.nostratech.project.persistence.repository.AccessLevelRepository;
import com.nostratech.project.persistence.repository.PrivilegeRepository;
import com.nostratech.project.persistence.domain.*;
import com.nostratech.project.persistence.repository.*;
import com.nostratech.project.oauth.provider.UserPasswordAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.List;

/**
 * agus w on 3/3/16.
 */
@Component
public class InitDB {

    public static final Logger logger = LoggerFactory.getLogger(InitDB.class);

    @Autowired
    ParameterRepository parameterRepository;

    @Autowired
    PrivilegeRepository privilegeRepository;

    @Autowired
    AccessLevelRepository accessLevelRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProfileAdminRepository profileAdminRepository;

    @Autowired
    @Qualifier("passwordEncoder")
    PasswordEncoder passwordEncoder;

    @Value("#{'${params.code}'.split(',')}") List<String> paramsCode;

    @Value("#{'${params.desc}'.split(',')}") List<String> paramsDesc;

    @Value("#{'${params.value}'.split(',')}") List<String> paramsValue;

    @Value("#{'${privilege.accessLevelUser}'.split(',')}") List<String> accessLevelUser;

    @Value("#{'${privilege.accessLevelUserDesc}'.split(',')}") List<String>  accessLevelUserDesc;

    @Value("#{'${privilege.accessLevelGroup}'.split(',')}") List<String>  accessLevelGroup;

    @Value("#{'${privilege.accessLevelGroupDesc}'.split(',')}") List<String>  accessLevelGroupDesc;

    @Value("#{'${privilege.accessLevelReport}'.split(',')}") List<String>  accessLevelReport;

    @Value("#{'${privilege.accessLevelReportDesc}'.split(',')}") List<String>  accessLevelReportDesc;

    @Value("#{'${accessLevel.desc}'.split(',')}") List<String> accessLevelDesc;

    @Value("#{'${accessLevel.value}'.split(',')}") List<String> accessLevelValue;

    @Value("#{'${accessLevel.keyName}'.split(',')}") List<String> accessLevelKeyName;

    @Value("${admin.password}") String adminPassword;

    @Value("${admin.email}") String adminEmail;

    @Value("${admin.fullname}") String adminFullname;

    @Value("${admin.name}") String adminName;

    @Value("${group.name}") String groupName;

    @Value("${group.accessLevel}") String groupAccessLevel;

    @Value("${client.id.admin}") String clientIdAdmin;


    @Autowired
    AccountRepository accountRepository;

    @Autowired
    NostraficationSupplierRepository nostraficationSupplierRepository;

    @Autowired
    BadanUsahaRepository badanUsahaRepository;

    @Autowired
    BadanUsahaAktaRevisiRepository badanUsahaAktaRevisiRepository;

    @Autowired
    BadanUsahaJasaRepository badanUsahaJasaRepository;

    @Autowired
    BadanUsahaJenisRepository badanUsahaJenisRepository;

    @Autowired
    BadanUsahaModalRepository badanUsahaModalRepository;

    @Autowired
    BadanUsahaPriceRepository badanUsahaPriceRepository;

    @Autowired
    BadanUsahaUploadRepository badanUsahaUploadRepository;

    @Autowired
    ContractRepository contractRepository;

    @Autowired
    ContractHistoryRepository contractHistoryRepository;

    @Autowired
    IndustryTypeRepository industryTypeRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    MerekRepository merekRepository;

    @Autowired
    MerekUploadRepository merekUploadRepository;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    PaymentSettingRepository paymentSettingRepository;

    @Autowired
    ProjectPriceRepository projectPriceRepository;

    @Autowired
    ContractTypeRepository contractTypeRepository;

    @Autowired
    VoucherRepository voucherRepository;

    @PostConstruct
    public void init(){

        Authentication authentication = new UserPasswordAuthenticationToken("SYSTEM", "SYSTEM");
        SecurityContextHolder.getContext().setAuthentication(authentication);
        createPrivileges();
        initAccessLevel();
        initGroup();
        initAdmin();

        logger.info("---------------------------------------------------------------");
        logger.info("PARAMETER SIZE : {} + {} + {} ", paramsCode.size(),  paramsDesc.size(), paramsValue.size());
        for(int i=0; i<paramsCode.size(); i++){
            initParameter(paramsCode.get(i), paramsDesc.get(i), paramsValue.get(i));
        }
        logger.info("---------------------------------------------------------------");
//        createAdmin();
        initAccount();
        initBadanUsaha();
        initBadanUsahaAktaRevisi();
        initBadanUsahaJasa();
        initBadanUsahaJenis();
        initBadanUsahaModal();
        initBadanUsahaPrice();
        initBadanUsahaUpload();
        initContract();
        initContractHistory();
        initIndustryType();
        initInvoice();
        initMerek();
        initMerekUpload();
        initPayment();
        initPaymentSetting();
        initProjectPrice();
        initContractType();
        initVoucher();
    }

    private void initVoucher(){
        List<Voucher> resultList = voucherRepository.findAll();
        for (Voucher result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                voucherRepository.save(result);
            }
        }
    }

    private void initContractType(){
        List<ContractType> resultList = contractTypeRepository.findAll();
        for (ContractType result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreatedAt(saved.getCreationDate());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                contractTypeRepository.save(result);
            }
        }
    }

    private void initProjectPrice(){
        List<ProjectPrice> resultList = projectPriceRepository.findAll();
        for (ProjectPrice result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                projectPriceRepository.save(result);
            }
        }
    }

    private void initPaymentSetting(){
        List<PaymentSetting> resultList = paymentSettingRepository.findAll();
        for (PaymentSetting result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                paymentSettingRepository.save(result);
            }
        }
    }

    private void initPayment(){
        List<Payment> resultList = paymentRepository.findAll();
        for (Payment result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                paymentRepository.save(result);
            }
        }
    }

    private void initMerekUpload(){
        List<MerekUpload> resultList = merekUploadRepository.findAll();
        for (MerekUpload result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                merekUploadRepository.save(result);
            }
        }
    }

    private void initMerek(){
        List<Merek> resultList = merekRepository.findAll();
        for (Merek result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                merekRepository.save(result);
            }
        }
    }

    private void initInvoice(){
        List<Invoice> resultList = invoiceRepository.findAll();
        for (Invoice result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                invoiceRepository.save(result);
            }
        }
    }

    private void initIndustryType(){
        List<IndustryType> resultList = industryTypeRepository.findAll();
        for (IndustryType result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                industryTypeRepository.save(result);
            }
        }
    }

    private void initContractHistory(){
        List<ContractHistory> resultList = contractHistoryRepository.findAll();
        for (ContractHistory result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                contractHistoryRepository.save(result);
            }
        }
    }

    private void initContract(){
        List<Contract> resultList = contractRepository.findAll();
        for (Contract result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                contractRepository.save(result);
            }
        }
    }

    private void initBadanUsahaUpload(){
        List<BadanUsahaUpload> resultList = badanUsahaUploadRepository.findAll();
        for (BadanUsahaUpload result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                badanUsahaUploadRepository.save(result);
            }
        }
    }

    private void initBadanUsahaPrice(){
        List<BadanUsahaPrice> resultList = badanUsahaPriceRepository.findAll();
        for (BadanUsahaPrice result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                badanUsahaPriceRepository.save(result);
            }
        }
    }

    private void initBadanUsahaModal(){
        List<BadanUsahaModal> resultList = badanUsahaModalRepository.findAll();
        for (BadanUsahaModal result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                badanUsahaModalRepository.save(result);
            }
        }
    }

    private void initBadanUsahaJenis(){
        List<BadanUsahaJenis> resultList = badanUsahaJenisRepository.findAll();
        for (BadanUsahaJenis result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                badanUsahaJenisRepository.save(result);
            }
        }
    }

    private void initBadanUsahaJasa(){
        List<BadanUsahaJasa> resultList = badanUsahaJasaRepository.findAll();
        for (BadanUsahaJasa result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                badanUsahaJasaRepository.save(result);
            }
        }
    }

    private void initBadanUsahaAktaRevisi(){
        List<BadanUsahaAktaRevisi> resultList = badanUsahaAktaRevisiRepository.findAll();
        for (BadanUsahaAktaRevisi result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                badanUsahaAktaRevisiRepository.save(result);
            }
        }
    }

    private void initBadanUsaha(){
        List<BadanUsaha> resultList = badanUsahaRepository.findAll();
        for (BadanUsaha result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                badanUsahaRepository.save(result);
            }
        }
    }

    private void initAccount(){
        List<Account> resultList = accountRepository.findAll();
        for (Account result : resultList){
            if (result.getDeleted() == null ||
                    result.getCreatedBy() == null ||
                    result.getCreationDate() == null ||
                    result.getSecureId() == null ||
                    result.getVersion() == null){
                NostraficationSupplier nostraficationSupplier = new NostraficationSupplier();
                NostraficationSupplier saved = nostraficationSupplierRepository.save(nostraficationSupplier);
                result.setCreatedBy(saved.getCreatedBy());
                result.setCreationDate(saved.getCreationDate());
                result.setDeleted(saved.getDeleted());
                result.setModifiedBy(saved.getModifiedBy());
                result.setModificationDate(saved.getModificationDate());
                result.setSecureId(saved.getSecureId());
                result.setVersion(saved.getVersion());
                accountRepository.save(result);
            }
        }
    }

    private void initGroup() {
        Group group = groupRepository.findByNameLike(groupName);
        if(group == null){
            group = new Group();
        }
        AccessLevel accessLevel = accessLevelRepository.findByValue(groupAccessLevel);
        group.setName(groupName);
        group.setAccessLevel(accessLevel);
        Collection<Privilege> privilegeCollections = privilegeRepository.findAll();
        group.setPrivileges(privilegeCollections);
        groupRepository.save(group);
    }

    private void initAdmin(){
        User user = userRepository.findByUsername(adminName);
        Group group = groupRepository.findByNameLike(groupName);
        if(user == null && group != null){
            user = new User();
            user.setGroup(group);
            user.setImmediateChangePassword(false);
            user.setEmail(adminEmail);
            user.setPassword(passwordEncoder.encode(adminPassword));
            user.setUsername(adminName);
            user.setClientId(clientIdAdmin);
            User saved = userRepository.save(user);
            ProfileAdmin profileAdmin = new ProfileAdmin();
            profileAdmin.setFullName(adminFullname);
            profileAdmin.setUser(saved);
            profileAdminRepository.save(profileAdmin);
        }
    }

    private void initAccessLevel(){

        List<AccessLevel> accessLevel = accessLevelRepository.findAll();
        if(accessLevel.size() == 0 ){
            logger.info("CREATE ACCESS LEVEL : {} + {} ", accessLevelDesc.size(),  accessLevelValue.size());
            if(accessLevelDesc.size() == accessLevelValue.size()){

                int[] idx = { 0 };
                accessLevelDesc.forEach(item -> {
                    AccessLevel newAccessLevel = new AccessLevel();
                    newAccessLevel.setInstance(item);
                    newAccessLevel.setValue(accessLevelValue.get(idx[0]));
                    newAccessLevel.setKeyName(accessLevelKeyName.get(idx[0]));
                    idx[0]++;
                    accessLevelRepository.saveAndFlush(newAccessLevel);
                });
            }else{
                logger.info("ACCESS LEVEL DESC AND VALUE NOT MATCH !!!");
            }
        }
    }

    private void initParameter(String code, String desc, String value) {
            logger.info("Checking Parameter code {}, desc {}, value {}", code, desc, value);

        Parameter parameter = parameterRepository.findByCode(code);

        if(null == parameter) {
            parameter = new Parameter();
        }
            logger.info("Creating Parameter code {}, desc {}, value {}", code, desc, value);
            parameter.setCode(code);
            parameter.setDescription(desc);
            parameter.setValue(value);
            parameterRepository.save(parameter);

        logger.info("Checking Parameter code {}, desc {}, value {} Done", code, desc, value);
    }

    private void createPrivileges() {
        //privilege user
        int[] idx = { 0 };
        accessLevelUser.forEach((privilege) -> {
            Privilege privileges = privilegeRepository.findByName(privilege);
            if(privileges == null){
                privileges = new Privilege();
            }
            privileges.setName(privilege);
            privileges.setDescription(accessLevelUserDesc.get(idx[0]));
            privileges.setCategory(Constants.PrivilegeCategory.PRIVILEGE_USER);
            idx[0]++;
            privilegeRepository.saveAndFlush(privileges);
        });

        //privilege group
        idx[0] = 0;
        accessLevelGroup.forEach((privilege) -> {
            Privilege privileges = privilegeRepository.findByName(privilege);
            if(privileges == null) {
                privileges = new Privilege();
            }
            privileges.setDescription(accessLevelGroupDesc.get(idx[0]));
            privileges.setCategory(Constants.PrivilegeCategory.PRIVILEGE_GROUP);
            privileges.setName(privilege);
            idx[0]++;
            privilegeRepository.saveAndFlush(privileges);
        });

        //privilege report
        idx[0] = 0;
        accessLevelReport.forEach((privilege) -> {
            Privilege privileges = privilegeRepository.findByName(privilege);
            if(privileges == null) {
                privileges = new Privilege();
            }
            privileges.setDescription(accessLevelReportDesc.get(idx[0]));
            privileges.setCategory(Constants.PrivilegeCategory.PRIVILEGE_REPORT);
            privileges.setName(privilege);
            idx[0]++;
            privilegeRepository.saveAndFlush(privileges);
        });

    }

}
