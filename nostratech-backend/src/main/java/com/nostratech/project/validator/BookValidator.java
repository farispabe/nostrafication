package com.nostratech.project.validator;

import com.nostratech.project.vo.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * agus w
 */

@Component
public class BookValidator {

    public String validateAddBook(CreateBookVO createBookVO){

        if (StringUtils.isEmpty(createBookVO.getName())) {
            return "Book Name Id is required";
        }

        if (StringUtils.isEmpty(createBookVO.getAuthorId())) {
            return "Author Id is required";
        }

        return null;
    }



}
