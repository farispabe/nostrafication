package com.nostratech.project.validator;

import com.nostratech.project.vo.*;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Created by Renny on 4/27/16.
 */
@Component
public class DomainValidator {

    public String validateAddHeadQuarter(HeadQuarterVO headQuarter) {

        if (StringUtils.isEmpty(headQuarter.getCode())) {
            return "Headquarter Code is required";
        }

        if (StringUtils.isEmpty(headQuarter.getName())) {
            return "Headquarter name is required";
        }

        if (StringUtils.isEmpty(headQuarter.getAddress())) {
            return "Address is required";
        }

        return null;
    }

    public String validateAddMainDealer(MainDealerVO mainDealerVO) {

        if (StringUtils.isEmpty(mainDealerVO.getCode())) {
            return "Main Dealer Code is required";
        }

        if (StringUtils.isEmpty(mainDealerVO.getName())) {
            return "Main Dealer name is required";
        }

        if (StringUtils.isEmpty(mainDealerVO.getAddress())) {
            return "Address is required";
        }

        return null;
    }

}
