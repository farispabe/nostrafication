package com.nostratech.project.validator;

import com.nostratech.project.vo.AssignGroupVO;
import com.nostratech.project.vo.ChangePasswordRequestVO;
import com.nostratech.project.vo.ChangeProfileUserRequestVO;
import com.nostratech.project.vo.ChangeProfileRequestVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * agus w
 */

@Component
public class ProfileValidator {

    @Autowired
    RegistrationValidator registrationValidator;

    @Autowired
    @Qualifier("passwordEncoder")
    PasswordEncoder passwordEncoder;

    public String validateChangeProfileUser(ChangeProfileUserRequestVO changeProfileRequestVO){

        if (StringUtils.isEmpty(changeProfileRequestVO.getUserId())) {
            return "User Id is required";
        }

        if (StringUtils.isEmpty(changeProfileRequestVO.getFullName())) {
            return "Full Name is required";
        }

        if (StringUtils.isEmpty(changeProfileRequestVO.getEmail())) {
            return "Email is required";
        }

        if (null == changeProfileRequestVO.getActive()) {
            return "Active Status is required";
        }

        return null;
    }

    public String validateChangeProfile(ChangeProfileRequestVO changeProfileRequestVO){

        if (StringUtils.isEmpty(changeProfileRequestVO.getFullName())) {
            return "Full Name is required";
        }

        if (StringUtils.isEmpty(changeProfileRequestVO.getPhoneNumber())) {
            return "Phone Number is required";
        }

        if(!registrationValidator.isPhoneFormatValid(changeProfileRequestVO.getPhoneNumber())) {
            return "Phone Number is not valid";
        }

        return null;
    }

    public String validateAssignGroup(AssignGroupVO assignGroupVO){

        if (StringUtils.isEmpty(assignGroupVO.getUserId())) {
            return "User Id is required";
        }

        if (StringUtils.isEmpty(assignGroupVO.getGroupId())) {
            return "Group Id is required";
        }

        return null;
    }

    public String validateChangePassword(ChangePasswordRequestVO changePasswordRequestVO, String encodedOldPassword){

        if (StringUtils.isEmpty(changePasswordRequestVO.getOldPassword())) {
            return "Old Password is required";
        }

        if (StringUtils.isEmpty(changePasswordRequestVO.getNewPassword())) {
            return "New Password is required";
        }

        if (StringUtils.isEmpty(changePasswordRequestVO.getConfirmationPassword())) {
            return "Confirmation Password is required";
        }

        if (changePasswordRequestVO.getNewPassword().compareTo(changePasswordRequestVO.getConfirmationPassword()) != 0) {
            return "New Password and Confirmation Password are not equal";
        }

        if(!passwordEncoder.matches(changePasswordRequestVO.getOldPassword(), encodedOldPassword)){
            return "Your old password not match";
        }

        if (changePasswordRequestVO.getNewPassword().compareTo(changePasswordRequestVO.getOldPassword()) == 0) {
            return "New Password must be different from old password";
        }

        if (!registrationValidator.isPasswordFormatValid(changePasswordRequestVO.getNewPassword())) {
            return "New Password must contains a minimum of 8 characters contain a number and a symbol ! @ # $ % & ? - + = _";
        }

        if (!registrationValidator.isPasswordFormatValid(changePasswordRequestVO.getConfirmationPassword())) {
            return "Confirmation Password must contains a minimum of 8 characters contain a number and a symbol ! @ # $ % & ? - + = _";
        }

        return null;
    }


}
