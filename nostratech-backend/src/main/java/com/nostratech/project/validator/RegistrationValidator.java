package com.nostratech.project.validator;

import com.nostratech.project.vo.RegistrationClientRequestVO;
import com.nostratech.project.vo.RegistrationRequestVO;
import com.nostratech.project.persistence.repository.UserRepository;
import com.nostratech.project.vo.RegistrationUserRequestVO;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * agus w
 */

@Component
public class RegistrationValidator {

    @Value("${password.pattern}")
    String passwordPattern;

    @Value("${phone.pattern}")
    String phonePattern;

    @Autowired
    UserRepository userRepository;

    public String validateRegistrationUser(RegistrationUserRequestVO registrationRequest, String clientId) {
        String error = validate(registrationRequest, clientId);

        if (!StringUtils.isEmpty(error)) {
            return error;
        }

        if (StringUtils.isEmpty(registrationRequest.getFullName())) {
            return "Full Name is required";
        }

        if (StringUtils.isEmpty(registrationRequest.getUsername())) {
            return "Username is required";
        }

        if (StringUtils.isEmpty(registrationRequest.getGroupId())) {
            return "Group is required";
        }

        return null;
    }

    public String validateRegistration(RegistrationRequestVO registrationRequest, String clientId) {
        String error = validate(registrationRequest, clientId);

        if (!StringUtils.isEmpty(error)) {
            return error;
        }

        if (StringUtils.isEmpty(registrationRequest.getFullName())) {
            return "Full Name is required";
        }
        return null;
    }

    public String validatePhoneNumber(String phoneNumber){

        if (StringUtils.isEmpty(phoneNumber)) {
            return "Phone Number is required";
        }

        if (!isPhoneFormatValid(phoneNumber)) {
            return "Phone Number is not valid";
        }

        return "ok";

    }


    public String validate(RegistrationClientRequestVO registrationRequest, String clientId) {

        if (StringUtils.isEmpty(registrationRequest.getEmail())) {
            return "Email is required";
        }

        if (!EmailValidator.getInstance().isValid(registrationRequest.getEmail())) {
            return "Email is not valid";
        }

        if (isEmailRegistered(registrationRequest.getEmail())) {
            return "Email is already registered";
        }

        return null;
    }

    private boolean isEmailRegistered(String email) {

        Object object = null;
        object = userRepository.findByEmail(email);
        return (null != object);
    }

    public boolean isPasswordFormatValid(String password) {
        return password.matches(passwordPattern);
    }

    public boolean isPhoneFormatValid(String phone) {
        return phone.matches(phonePattern);
    }

}
