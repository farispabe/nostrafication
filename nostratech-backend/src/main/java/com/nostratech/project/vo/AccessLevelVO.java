package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by PanjiAlam on 8/16/2016.
 */
@Data
public class AccessLevelVO extends BaseVO{
    String instance;
    String value;
    String keyName;
}
