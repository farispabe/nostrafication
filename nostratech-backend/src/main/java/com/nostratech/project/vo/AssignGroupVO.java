package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by rennytanuwijaya on 4/15/16.
 */
@Data
public class AssignGroupVO {

    private Boolean force;
    private String userId;
    private String groupId;

}
