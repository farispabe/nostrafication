package com.nostratech.project.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by rennytanuwijaya on 4/15/16.
 */
@Data
public class AssignPrivilegeVO {
   private String groupId;
   private String newName;
   private List<BaseVO> privilegeList;

}
