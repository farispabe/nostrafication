package com.nostratech.project.vo;

import lombok.Data;

import java.util.Collection;

/**
 * Created by CiTruS on 8/21/2014.
 */
@Data
public class AuthorVO extends BaseVO {

    private String name;
    private String gender;
    private Collection<BookVO> bookVOList;
}
