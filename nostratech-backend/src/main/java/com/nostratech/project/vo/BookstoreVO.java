package com.nostratech.project.vo;

import lombok.Data;

import java.util.Collection;

/**
 * Created by CiTruS on 8/21/2014.
 */
@Data
public class BookstoreVO extends BaseVO {

    private String name;
    private String address;
    private Collection<BookVO> bookVOList;
}
