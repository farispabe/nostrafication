package com.nostratech.project.vo;

import lombok.Data;

/**
 * agus w
 */
@Data
public class ChangePasswordRequestVO {

    private String oldPassword;
    private String newPassword;
    private String confirmationPassword;
}
