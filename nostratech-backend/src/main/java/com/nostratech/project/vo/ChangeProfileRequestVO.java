package com.nostratech.project.vo;

import lombok.Data;

/**
 * agus w
 */
@Data
public class ChangeProfileRequestVO extends ChangeProfileUserRequestVO {

    private String phoneNumber;
}
