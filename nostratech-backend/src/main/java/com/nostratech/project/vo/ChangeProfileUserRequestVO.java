package com.nostratech.project.vo;

import lombok.Data;

/**
 * agus w
 */
@Data
public class ChangeProfileUserRequestVO {

    private String userId;
    private String fullName;
    private String email;
    private Boolean active;
}
