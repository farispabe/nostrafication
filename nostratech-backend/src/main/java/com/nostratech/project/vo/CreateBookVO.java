package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by CiTruS on 8/21/2014.
 */
@Data
public class CreateBookVO extends BaseVO {

    private String name;
    private String authorId;
}
