package com.nostratech.project.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by CiTruS on 8/21/2014.
 */
@Data
public class CreateBookstoreVO extends BaseVO {

    private String name;
    private String address;
    private List<BookVO> bookIdList;
}
