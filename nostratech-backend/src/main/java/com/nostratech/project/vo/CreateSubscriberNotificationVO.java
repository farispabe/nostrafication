package com.nostratech.project.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by Renny on 6/30/16.
 */
@Data
public class CreateSubscriberNotificationVO {
    private List<BaseVO> masterNotificationId;
    private String groupId;
}
