package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by PanjiAlam on 8/29/2016.
 */
@Data
public class DetailUserVO extends BaseVO {
    private String username;
    private Boolean active;
    private String groupId;
    private String email;
    private String fullName;
}
