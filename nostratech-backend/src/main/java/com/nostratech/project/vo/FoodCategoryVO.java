package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by Renny on 04/02/18.
 */

@Data
public class FoodCategoryVO extends BaseVO{
    private String nameCategory;
}
