package com.nostratech.project.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by Renny on 8/19/16.
 */
@Data
public class GroupDetailVO extends GroupVO {
    private List<MasterNotificationVO> masterNotificationVOs;
}
