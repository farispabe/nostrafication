package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by Renny on 8/22/16.
 */
@Data
public class GroupListVO extends BaseVO {
    private String name;
    private String accessLevel;
}
