package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by rennytanuwijaya on 4/13/16.
 */
@Data
public class GroupPrivilegeVO{
    String groupId;
    String privilegeID;
}