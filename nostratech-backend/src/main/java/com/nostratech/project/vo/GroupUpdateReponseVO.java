package com.nostratech.project.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by Renny on 8/30/16.
 */
@Data
public class GroupUpdateReponseVO  extends BaseVO {

    private String name;
    private String accessLevel;
    private List<PrivilegeListVO> privilegeList;

}
