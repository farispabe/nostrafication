package com.nostratech.project.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by rennytanuwijaya on 4/13/16.
 */
@Data
public class GroupVO extends BaseVO {

    private String name;
    private String accessLevel;
    private List<PrivilegeVO> privilegeList;

}
