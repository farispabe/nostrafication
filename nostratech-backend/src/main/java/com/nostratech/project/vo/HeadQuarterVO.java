package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by Renny on 4/26/16.
 */
@Data
public class HeadQuarterVO extends BaseVO{

    private String code;
    private String name;
    private String address;
}
