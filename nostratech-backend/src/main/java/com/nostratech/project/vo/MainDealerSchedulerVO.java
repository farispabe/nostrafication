package com.nostratech.project.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by derryaditiya on 10/3/16.
 */
@Data
public class MainDealerSchedulerVO {

    private List<MainDealerVO> listMainDealer;

}
