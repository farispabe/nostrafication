package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by Renny on 4/27/16.
 */
@Data
public class MainDealerVO extends BaseVO{

    private String code;
    private String name;
    private String address;
}
