package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by Renny on 8/19/16.
 */
@Data
public class MasterNotificationVO extends BaseVO {
    private String notificationKey;
    private Boolean value;
}
