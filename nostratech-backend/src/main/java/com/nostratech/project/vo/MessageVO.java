package com.nostratech.project.vo;

import lombok.Data;

/**
 * agus w on 2/23/16.
 */
@Data
public class MessageVO {

    private String header;
    private String body;

    public MessageVO(String header, String body){
        this.header = header;
        this.body = body;
    }
}
