package com.nostratech.project.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by PanjiAlam on 8/19/2016.
 */
@Data
public class NewAccessLevelVO {
    private List<AccessLevelVO> newAccessLevel;
}
