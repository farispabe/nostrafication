package com.nostratech.project.vo;

import lombok.Data;
import java.util.Date;

/**
 * Created by Renny on 6/30/16.
 */
@Data
public class NewNotificationVO{

    private String notificationKey;
    private String url;
    private String message;
    private Date expiredDate;
    private Date creationDate;
}
