package com.nostratech.project.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by Renny on 6/30/16.
 */
@Data
public class NotificationMessageVO {
    private List<NewNotificationVO> newNotificationList;
}
