package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by PanjiAlam on 8/29/2016.
 */
@Data
public class ParameterVO extends BaseVO{
    String code;
    String value;
    String description;
}
