package com.nostratech.project.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by Renny on 8/30/16.
 */
@Data
public class PrivilegeListVO {
    private String category;
    private Boolean value;
    private List<PrivilegeVO> privilegeVOList;
}
