package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by Renny on 8/30/16.
 */
@Data
public class PrivilegeNameVO {
    private String name;
}
