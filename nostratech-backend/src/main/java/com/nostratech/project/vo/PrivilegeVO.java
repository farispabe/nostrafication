package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by rennytanuwijaya on 4/12/16.
 */
@Data
public class PrivilegeVO extends BaseVO {
    private String name;
    private String description;
    private Boolean value;
}
