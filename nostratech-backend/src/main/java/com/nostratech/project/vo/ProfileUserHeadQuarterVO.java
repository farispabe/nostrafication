package com.nostratech.project.vo;
import lombok.Data;

/**
 * Created by Renny on 4/29/16.
 */
@Data
public class ProfileUserHeadQuarterVO extends BaseVO {
    private UserVO user;
    private String domainName;
    private String fullName;
    private String phoneNumber;
}
