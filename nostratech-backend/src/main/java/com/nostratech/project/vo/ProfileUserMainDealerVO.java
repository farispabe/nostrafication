package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by Renny on 4/30/16.
 */
@Data
public class ProfileUserMainDealerVO extends BaseVO {
        private UserVO user;
        private String domainName;
        private String fullName;
        private String phoneNumber;
}
