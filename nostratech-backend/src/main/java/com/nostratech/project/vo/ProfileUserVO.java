package com.nostratech.project.vo;

import lombok.Data;

/**
 * agus w
 */
@Data
public class ProfileUserVO{

    private String email;
    private String fullName;
    private String address;

}
