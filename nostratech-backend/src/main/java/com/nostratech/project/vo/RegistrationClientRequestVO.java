package com.nostratech.project.vo;

import lombok.Data;

/**
 * agus w
 */
@Data
public class RegistrationClientRequestVO {

    private String email;
    private String username;
    private String password;
    private String confirmationPassword;

}
