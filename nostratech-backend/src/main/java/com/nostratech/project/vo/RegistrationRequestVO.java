package com.nostratech.project.vo;

import lombok.Data;

/**
 * agus w
 */

@Data
public class RegistrationRequestVO extends RegistrationUserRequestVO{

    private String phoneNumber;
}
