package com.nostratech.project.vo;

import lombok.Data;

/**
 * agus w
 */
@Data
public class RegistrationResponseVO {

    private String accessToken;
}
