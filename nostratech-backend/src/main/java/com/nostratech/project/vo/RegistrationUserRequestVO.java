package com.nostratech.project.vo;

import lombok.Data;

/**
 * agus w
 */
@Data
public class RegistrationUserRequestVO extends RegistrationClientRequestVO {

    //user data
    private String fullName;
    private String role;
    private String groupId;

}
