package com.nostratech.project.vo;

import lombok.Data;

/**
 * agus w
 */
@Data
public class ResetPasswordVO {

    private String email;
}
