package com.nostratech.project.vo;

import lombok.Data;

/**
 * agus w
 */
@Data
public class SignInRequestVO {

//    private String email;
    private String username;
    private String password;
}
