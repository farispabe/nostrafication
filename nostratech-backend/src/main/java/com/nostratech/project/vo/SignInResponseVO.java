package com.nostratech.project.vo;

import lombok.Data;

/**
 * agus w
 */
@Data
public class SignInResponseVO {

    private String accessToken;

    private Boolean immediateChangePassword;

    private GroupSignInVO group;

    private String fullName;

    private String domain;

}
