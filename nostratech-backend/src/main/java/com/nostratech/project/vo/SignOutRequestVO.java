package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by derryaditiya on 5/18/16.
 */

@Data
public class SignOutRequestVO {

    private String accessToken;

}
