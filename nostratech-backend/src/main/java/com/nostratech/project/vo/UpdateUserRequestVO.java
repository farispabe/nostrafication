package com.nostratech.project.vo;

import lombok.Data;

/**
 * Created by PanjiAlam on 8/29/2016.
 */
@Data
public class UpdateUserRequestVO extends BaseVO{
    String email;
    Boolean active;
    String fullName;
    String phoneNumber;
    String groupId;
}
