package com.nostratech.project.vo;
import lombok.Data;

import java.util.Date;

/**
 * Created by Renny on 4/27/16.
 */
@Data
public class UserVO extends BaseVO {
    private String email;
    private Boolean active;
    private String username ;
    private Boolean immediateChangePassword;
    private GroupVO group;
    private Date lastUpdatedDate;

}
