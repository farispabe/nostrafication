package com.nostratech.project.vo.mail;

import lombok.Data;

import java.util.Map;

@Data
public class BaseEmailVO {

    private String sender;
    private String subject;
    private String templateLocation;
    private Map templateValues;
}
