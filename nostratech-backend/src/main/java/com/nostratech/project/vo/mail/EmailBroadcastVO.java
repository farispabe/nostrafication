package com.nostratech.project.vo.mail;

import lombok.Data;

import java.util.List;

@Data
public class EmailBroadcastVO extends BaseEmailVO {

    private List<String> destinations;
}
