package com.nostratech.project.vo.mail;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.io.IOException;

/**
 * agus w
 */
@Data
public class EmailVO extends BaseEmailVO {

    private String destination;

    @JsonCreator
    public static EmailVO Create(String jsonString) throws IOException, JsonParseException, JsonMappingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonString, EmailVO.class);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("EmailVO{");
        sb.append("sender='").append(getSender()).append('\'');
        sb.append(", subject='").append(getSubject()).append('\'');
        sb.append(", destination='").append(getDestination()).append('\'');
        sb.append(", templateLocation='").append(getTemplateLocation()).append('\'');
        sb.append(", templateValues=").append(getTemplateValues());
        sb.append('}');
        return sb.toString();
    }
}
