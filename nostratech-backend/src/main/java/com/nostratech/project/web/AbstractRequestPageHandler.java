package com.nostratech.project.web;

import com.nostratech.project.exception.NostraException;
import com.nostratech.project.util.RestUtil;
import com.nostratech.project.vo.ResultPageVO;
import org.springframework.http.ResponseEntity;

/**
 * agus w
 */
public abstract class AbstractRequestPageHandler {

    public ResponseEntity<ResultPageVO> getResult() {
        ResultPageVO result = new ResultPageVO();
        try {
            return processRequest();
        } catch (NostraException e) {
            result.setMessage(e.getCode().name());
            result.setResult(e.getMessage());
        }
        return RestUtil.getJsonResponse(result);
    }

    public abstract ResponseEntity<ResultPageVO> processRequest();
}
