package com.nostratech.project.web;


import com.nostratech.project.service.AccessLevelService;
import com.nostratech.project.vo.ResultVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by PanjiAlam on 8/16/2016.
 */


@RestController
@RequestMapping ("/api/access-level")
public class AccessLevelController {

    @Autowired
    AccessLevelService accessLevelService;

    public static final Logger logger = LoggerFactory.getLogger(AccessLevelController.class);


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getAccessLevel(@RequestParam(value = "id", required = false, defaultValue = "")  String secureId) {
       AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return accessLevelService.getAccessLevel(secureId);
            }

        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/list-of-access-level",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getListOfAccessLevel() {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return accessLevelService.getListOfAccessLevel();
            }
        };
        return handler.getResult();
    }
}
