package com.nostratech.project.web;

import com.nostratech.project.service.SignInService;
import com.nostratech.project.vo.ResultVO;
import com.nostratech.project.vo.SignInRequestVO;
import com.nostratech.project.vo.SignOutRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * agus w
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    @Autowired
    SignInService signInService;

    @PreAuthorize("permitAll")
    @RequestMapping(value = "/logout",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> logout(
            @RequestBody SignOutRequestVO signOutRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return signInService.logout(signOutRequestVO);
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("permitAll")
    @RequestMapping(value = "/login",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> login(@RequestBody SignInRequestVO signInRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return signInService.loginUser(signInRequestVO);
            }
        };
        return handler.getResult();
    }
}
