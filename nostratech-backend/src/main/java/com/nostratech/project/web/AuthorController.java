package com.nostratech.project.web;

import com.nostratech.project.service.AuthorService;
import com.nostratech.project.service.BookService;
import com.nostratech.project.vo.CreateAuthorVO;
import com.nostratech.project.vo.CreateBookVO;
import com.nostratech.project.vo.ResultVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by rennytanuwijaya on 4/11/16.
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/author")
public class AuthorController {

    public static final Logger logger = LoggerFactory.getLogger(AuthorController.class);

    @Autowired
    AuthorService authorService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/create-author",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> addBook(@RequestBody CreateAuthorVO authorVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return authorService.addAuthor(authorVO);
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/list-of-author",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getListOfAuthor() {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return authorService.getListOfAuthor();
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/get-detail",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getDetailAuthor(@RequestParam("id") String id) {
        AbstractRequestHandler handler = new AbstractRequestHandler(){
        @Override
            public Object processRequest() {
                return authorService.getDetail(id);
            }
        };
        return handler.getResult();
    }


}
