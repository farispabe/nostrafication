package com.nostratech.project.web;

import com.nostratech.project.service.BookService;
import com.nostratech.project.service.PrivilegeService;
import com.nostratech.project.vo.BookVO;
import com.nostratech.project.vo.CreateBookVO;
import com.nostratech.project.vo.ResultVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by rennytanuwijaya on 4/11/16.
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/book")
public class BookController {

    public static final Logger logger = LoggerFactory.getLogger(BookController.class);

    @Autowired
    BookService bookService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/create-book",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> addBook(@RequestBody CreateBookVO bookVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return bookService.addBook(bookVO);
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/create-book-dumy",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> addBookDummy(@RequestBody CreateBookVO bookVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return bookService.addBook(bookVO);
            }
        };
        return handler.getResult();
    }


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/list-of-book",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getListOfBook() {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return bookService.getListOfBook();
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/get-detail",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getDetailBook(@RequestParam("id") String id) {
        AbstractRequestHandler handler = new AbstractRequestHandler(){
        @Override
            public Object processRequest() {
                return bookService.getDetail(id);
            }
        };
        return handler.getResult();
    }


}
