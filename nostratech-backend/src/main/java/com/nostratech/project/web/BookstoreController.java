package com.nostratech.project.web;

import com.nostratech.project.service.BookService;
import com.nostratech.project.service.BookstoreService;
import com.nostratech.project.vo.CreateBookVO;
import com.nostratech.project.vo.CreateBookstoreVO;
import com.nostratech.project.vo.ResultVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by rennytanuwijaya on 4/11/16.
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/bookstore")
public class BookstoreController {

    public static final Logger logger = LoggerFactory.getLogger(BookstoreController.class);

    @Autowired
    BookstoreService bookstoreService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/create-bookstore",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> addBook(@RequestBody CreateBookstoreVO bookstoreVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return bookstoreService.addBookStore(bookstoreVO);
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/list-of-bookstore",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getListOfBookstore() {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return bookstoreService.getListOfBookStore();
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/get-detail",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getDetailBook(@RequestParam("id") String id) {
        AbstractRequestHandler handler = new AbstractRequestHandler(){
        @Override
            public Object processRequest() {
                return bookstoreService.getDetail(id);
            }
        };
        return handler.getResult();
    }


}
