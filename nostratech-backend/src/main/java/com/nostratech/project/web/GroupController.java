package com.nostratech.project.web;

import com.nostratech.project.persistence.domain.Group;
import com.nostratech.project.service.GroupService;
import com.nostratech.project.vo.AssignPrivilegeVO;
import com.nostratech.project.vo.DeleteGroupRequestVO;
import com.nostratech.project.vo.GroupVO;
import com.nostratech.project.service.BaseVoService;
import com.nostratech.project.vo.ResultPageVO;
import com.nostratech.project.vo.ResultVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by rennytanuwijaya on 4/13/16.
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/group")
public class GroupController extends BaseController<Group, GroupVO, GroupVO> {

    public static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    @Autowired
    GroupService groupService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/create-group",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> addGroup(@RequestBody GroupVO groupVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return groupService.addGroup(groupVO);
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/assign-privilege",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> assignPrivilegeToGroup(@RequestBody AssignPrivilegeVO group) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return groupService.assignPrivilegeToGroup(group);
            }
        };
        return handler.getResult();
    }


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/get-group-detail",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getGroup(@RequestParam("groupsId") String group) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return groupService.getGroup(group);
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/list-of-group-only",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getListOfGroupForSpesificAccessLevel(@RequestParam("accessLevel") String accessLevel) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return groupService.getListOfGroup(accessLevel);
            }
        };
        return handler.getResult();
    }


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/group-detail-for-update",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getGroupForUpdate(@RequestParam("groupId") String groupId) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return groupService.getGroupForUpdate(groupId);
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "delete-group",
            method = RequestMethod.DELETE,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> deleteAlbum(
            @RequestBody DeleteGroupRequestVO deleteGroupRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return groupService.deleteGroup(deleteGroupRequestVO);
            }
        };
        return handler.getResult();
    }


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/update-privilege",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> updateGroup(@RequestBody AssignPrivilegeVO groupVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return groupService.updatePrivilege(groupVO);
            }
        };
        return handler.getResult();
    }

    @RequestMapping(value = "/list-of-group",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultPageVO> getListOfGroup(@RequestParam(value = "page", required = true, defaultValue = "0") Integer page,
                                                       @RequestParam(value = "limit", required = true, defaultValue = "10") Integer limit,
                                                       @RequestParam(value = "sortBy", required = false) String sortBy,
                                                       @RequestParam(value = "direction", required = false) String direction,
                                                       @RequestParam(value = "name", required = false,defaultValue = "") String name){
        Map<String, Object> pageMap = groupService.listGroup(page, limit, sortBy, direction,name);
        return constructListResult(pageMap);
    }


    @Override
    protected BaseVoService<Group, GroupVO, GroupVO> getDomainService() {
        return null;
    }
}
