package com.nostratech.project.web;

import com.nostratech.project.service.ParameterService;
import com.nostratech.project.vo.ResultVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by PanjiAlam on 8/29/2016.
 */

@RestController
@RequestMapping("/api/parameters")
public class ParameterController {
    @Autowired
    ParameterService parameterService;

    public static final Logger logger = LoggerFactory.getLogger(AccessLevelController.class);
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getListOfParameter() {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return parameterService.getParameter();
            }
        };
        return handler.getResult();
    }

}
