package com.nostratech.project.web;

import com.nostratech.project.vo.PrivilegeVO;
import com.nostratech.project.vo.ResultVO;
import com.nostratech.project.service.PrivilegeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by rennytanuwijaya on 4/11/16.
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/privilege")
public class PrivilegeController {

    public static final Logger logger = LoggerFactory.getLogger(PrivilegeController.class);

    @Autowired
    PrivilegeService privilegeService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/list-of-privilege",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getListOfPrivilege() {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return privilegeService.getListOfPrivilege();
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/user-privilege",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> userPrivilege() {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return privilegeService.userPrivilege();
            }
        };
        return handler.getResult();
    }
}
