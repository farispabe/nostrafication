package com.nostratech.project.web;

import com.nostratech.project.vo.AssignGroupVO;
import com.nostratech.project.vo.ChangePasswordRequestVO;
import com.nostratech.project.vo.ChangeProfileRequestVO;
import com.nostratech.project.vo.ChangeProfileUserRequestVO;
import com.nostratech.project.vo.*;
import com.nostratech.project.service.ProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * agus w
 */

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/profiles")
public class ProfileController{

    public static final Logger logger = LoggerFactory.getLogger(ProfileController.class);

    @Autowired
    ProfileService profileService;

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/user",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getUser() {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return profileService.getUserProfile();
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/user",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> editUser(@RequestBody ChangeProfileUserRequestVO profileRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return profileService.editUser(profileRequestVO);
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/user/change-password",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> changePasswordUser(@RequestBody ChangePasswordRequestVO changePasswordRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return profileService.changePasswordUser(changePasswordRequestVO);
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/user/assign-group",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> assignGroupToUser(@RequestBody AssignGroupVO assignGroupVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return profileService.assignGroup(assignGroupVO);
            }
        };
        return handler.getResult();
    }
}
