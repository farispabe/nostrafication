package com.nostratech.project.web;

import com.nostratech.project.service.RegistrationService;
import com.nostratech.project.vo.RegistrationRequestVO;
import com.nostratech.project.vo.RegistrationUserRequestVO;
import com.nostratech.project.vo.ResetPasswordVO;
import com.nostratech.project.vo.ResultVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * agus w
 */

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/registration")
public class RegistrationController {

    public static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    RegistrationService registrationService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/user",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> registrationUser(@RequestBody RegistrationUserRequestVO userRegistration) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return registrationService.registrationUser(userRegistration);
            }
        };
        return handler.getResult();
    }


    @RequestMapping(value = "/user/{email}/", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ResultVO> checkUser(@PathVariable("email") String email) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return registrationService.checkUserEmail(email);
            }
        };
        return handler.getResult();
    }


    @RequestMapping(value = "/user/reset-password", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ResultVO> resetPasswordUser(@RequestBody final ResetPasswordVO resetPassword) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return registrationService.resetPasswordUser(resetPassword);
            }
        };
        return handler.getResult();
    }

}
