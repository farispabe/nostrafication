package com.nostratech.project.web;

import com.nostratech.project.vo.DeleteUserRequestVO;
import com.nostratech.project.vo.UpdateUserRequestVO;
import com.nostratech.project.service.BaseVoService;
import com.nostratech.project.persistence.domain.User;
import com.nostratech.project.service.UsersService;
import com.nostratech.project.vo.ResultPageVO;
import com.nostratech.project.vo.UserVO;
import com.nostratech.project.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by Renny on 4/29/16.
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/user")
public class UserController  extends BaseController<User, UserVO, UserVO> {

    @Autowired
    UsersService userService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/delete-user",
            method = RequestMethod.DELETE,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> deleteUser(
            @RequestBody DeleteUserRequestVO deleteUserRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return userService.deleteUser(deleteUserRequestVO);
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/get-user",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getUserCoba(@RequestParam("id") String id) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return userService.getDetailsUser(id);
            }
        };
        return handler.getResult();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/update-user",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> updateUser(@RequestBody UpdateUserRequestVO updateUserRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return userService.updateUser(updateUserRequestVO);
            }
        };
        return handler.getResult();
    }


    @Override
    protected BaseVoService<User, UserVO, UserVO> getDomainService() {
        return null;
    }

}
