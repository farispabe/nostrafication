<%@ page
        import="org.springframework.security.core.AuthenticationException"%>
<%@ page
        import="org.springframework.security.oauth2.common.exceptions.UnapprovedClientAuthenticationException"%>
<%@ page
        import="org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter"%>
<%@ taglib prefix="authz"
           uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<authz:authorize ifAnyGranted="ROLE_ADMIN">
    <c:redirect url="index.jsp" />
</authz:authorize>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Backend API</title>
</head>
<body>

<h1>Backend API</h1>

<div id="content">
    <%
        if (session
                .getAttribute(AbstractAuthenticationProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY) != null
                && !(session
                .getAttribute(AbstractAuthenticationProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY) instanceof UnapprovedClientAuthenticationException)) {
    %>
    <div class="error">
        <h2>Woops!</h2>

        <p>
            Your login attempt was not successful. (<%=((AuthenticationException) session
                .getAttribute(AbstractAuthenticationProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY))
                .getMessage()%>)
        </p>
    </div>
    <%
        }
    %>
    <c:remove scope="session" var="SPRING_SECURITY_LAST_EXCEPTION" />

    <authz:authorize ifNotGranted="ROLE_ADMIN">
        <h2>Login</h2>

        <form id="loginForm" name="loginForm"
              action="<c:url value="/login.do"/>" method="post">
            <p>
                <label>Username: <input type='text' name='j_username'
                                        value="" /></label>
            </p>
            <p>
                <label>Password: <input type="password" name='j_password'
                                        value="" /></label>
            </p>

            <p>
                <input name="login" value="Login" type="submit" />
            </p>
        </form>
    </authz:authorize>
</div>

<div id="footer">
    <!-- <a href="pages/register.jsp" >register</a>  -->
</div>

<!-- <div id="footer">
    Sample application for <a
        href="http://github.com/SpringSource/spring-security-oauth"
        target="_blank">Spring Security OAuth</a>
</div> -->


</body>
</html>
